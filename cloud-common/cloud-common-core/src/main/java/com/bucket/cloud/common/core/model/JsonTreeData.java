package com.bucket.cloud.common.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class JsonTreeData implements GrantedAuthority {

    private static final long serialVersionUID = 2530593347982042435L;

    private String id;       //json id

    private String pid;      //

    private String text;     //json 显示文本

    private String state;    //json 'open','closed'

    private int available;

    private Attributes attributes;

    private List<JsonTreeData> children;       //

    @Override
    public String getAuthority() {
        return this.id;
    }
}