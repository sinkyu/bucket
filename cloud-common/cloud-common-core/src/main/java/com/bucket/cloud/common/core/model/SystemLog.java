package com.bucket.cloud.common.core.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.sql.Timestamp;

/**
 * code is far away from bug with the animal protecting
 * ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃代码无BUG！
 * 　　┃　　　┗━━━┓
 * 　　┃　　　　　　　┣┓
 * 　　┃　　　　　　　┏┛
 * 　　┗┓┓┏━┳┓┏┛
 * 　　　┃┫┫　┃┫┫
 * 　　　┗┻┛　┗┻┛
 *
 * @Description : 日志表实体类
 * ---------------------------------
 * @Author : cyq
 * @Date : Create in 2018/11/20 10:20
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "SystemLog", description = "日志实体类")
@TableName("SYSTEM_LOG")
public class SystemLog implements Serializable {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "日志切入点")
    @TableField(value = "JOINT_PT")
    private String jointPt;

    @ApiModelProperty(value = "日志描述")
    private String remark;

    @ApiModelProperty(value = "日志名称")
    private String name;

    @ApiModelProperty(value = "日志类型")
    private String type;

    @ApiModelProperty(value = "请求参数")
    private String params;

    @ApiModelProperty(value = "请求IP")
    @TableField(value = "REQ_IP")
    private String reqIp;

    @ApiModelProperty(value = "请求结果")
    private String results;

    @ApiModelProperty(value = "总耗时")
    @TableField(value = "TOTAL_TIME")
    private Long totalTime;

    @ApiModelProperty(value = "用户ID")
    @TableField(value = "USER_ID")
    private String userId;

    @ApiModelProperty(value = "创建时间")
    @TableField(value = "CREATE_TIME")
    private Timestamp createTime;
}
