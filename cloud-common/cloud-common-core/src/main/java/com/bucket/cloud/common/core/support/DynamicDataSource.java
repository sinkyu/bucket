//package com.bucket.cloud.common.core.support;
//
//import org.springframework.jdbc.datasource.lookup.AbstractRoutingDataSource;
//
///**
// * code is far away from bug with the animal protecting
// * ┏┓　　　┏┓
// * ┏┛┻━━━┛┻┓
// * ┃　　　　　　　┃
// * ┃　　　━　　　┃
// * ┃　┳┛　┗┳　┃
// * ┃　　　　　　　┃
// * ┃　　　┻　　　┃
// * ┃　　　　　　　┃
// * ┗━┓　　　┏━┛
// * 　　┃　　　┃神兽保佑
// * 　　┃　　　┃代码无BUG！
// * 　　┃　　　┗━━━┓
// * 　　┃　　　　　　　┣┓
// * 　　┃　　　　　　　┏┛
// * 　　┗┓┓┏━┳┓┏┛
// * 　　　┃┫┫　┃┫┫
// * 　　　┗┻┛　┗┻┛
// *
// * @Description : 数据源路由
// * ---------------------------------
// * @Author : cyq
// * @Date : Create in 2018/11/22 13:57
// */
//public class DynamicDataSource extends AbstractRoutingDataSource {
//    /**
//     * 取得当前使用哪个数据源
//     * @return
//     */
//    @Override
//    protected Object determineCurrentLookupKey() {
//        return DbContextHolder.getDbType();
//    }
//}
