package com.bucket.cloud.common.core.model;

import lombok.Data;

import java.util.Date;

/**
 * @Auther: cyq
 * @Date: 2019/8/29 22:55
 * @Description: 数据库主表的通用字段
 */
@Data
public class BasePojo {

    private String createBy;

    private String updateBy;

    private Date createTime;

    private Date updateTime;

    private Integer delFlag;

}
