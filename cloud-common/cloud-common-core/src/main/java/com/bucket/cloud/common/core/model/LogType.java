package com.bucket.cloud.common.core.model;

/**
 * code is far away from bug with the animal protecting
 * ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃代码无BUG！
 * 　　┃　　　┗━━━┓
 * 　　┃　　　　　　　┣┓
 * 　　┃　　　　　　　┏┛
 * 　　┗┓┓┏━┳┓┏┛
 * 　　　┃┫┫　┃┫┫
 * 　　　┗┻┛　┗┻┛
 *
 * @Description : 日志类型枚举
 * ---------------------------------
 * @Author : cyq
 * @Date : Create in 2018/11/20 09:58
 */
public enum LogType {
    LOGIN("login", "登录"),
    LOGOUT("logout", "登出"),
    ADD("add", "新增"),
    EDIT("edit", "修改"),
    DELETE("delete", "删除"),
    QUERY("query", "查询"),
    OTHER("other", "其他");

    private String type;
    private String desc;

    private LogType(String type, String desc){
        this.type = type;
        this.desc = desc;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}

