package com.bucket.cloud.common.core.exception;

import com.bucket.cloud.common.core.constants.ErrorCode;

/**
 * 基础错误异常
 *
 * @author admin
 */
public class BucketException extends RuntimeException {

    private static final long serialVersionUID = 3655050728585279326L;

    private int code = ErrorCode.ERROR.getCode();

    public BucketException() {

    }

    public BucketException(String msg) {
        super(msg);
    }

    public BucketException(int code, String msg) {
        super(msg);
        this.code = code;
    }

    public BucketException(int code, String msg, Throwable cause) {
        super(msg, cause);
        this.code = code;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }


}
