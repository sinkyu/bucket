package com.bucket.cloud.common.core.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Attributes implements Serializable{

    private static final long serialVersionUID = -8943790295458884292L;

    private String url;

    private String code;

    private String type;

    private String btnClass;
}
