package com.bucket.cloud.common.core.model;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@ApiModel(value = "Page", description = "分页")
@NoArgsConstructor
@AllArgsConstructor
public class Page {

    @ApiModelProperty(value = "第几页")
    private int pageNumber;

    @ApiModelProperty(value = "条数")
    private int pageSize;
}
