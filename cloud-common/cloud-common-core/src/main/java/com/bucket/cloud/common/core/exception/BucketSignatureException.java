package com.bucket.cloud.common.core.exception;

/**
 * 签名异常
 *
 * @author admin
 */
public class BucketSignatureException extends BucketException {
    private static final long serialVersionUID = 4908906410210213271L;

    public BucketSignatureException() {
    }

    public BucketSignatureException(String msg) {
        super(msg);
    }

    public BucketSignatureException(int code, String msg) {
        super(code, msg);
    }

    public BucketSignatureException(int code, String msg, Throwable cause) {
        super(code, msg, cause);
    }
}
