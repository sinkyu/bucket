package com.bucket.cloud.common.core.support;

import com.bucket.cloud.common.core.constants.QueueConstants;
import com.bucket.cloud.common.core.model.SystemLog;
import com.bucket.cloud.common.core.util.HttpKit;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Component;

import java.security.Principal;
import java.sql.Timestamp;

/**
 * code is far away from bug with the animal protecting
 * ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃代码无BUG！
 * 　　┃　　　┗━━━┓
 * 　　┃　　　　　　　┣┓
 * 　　┃　　　　　　　┏┛
 * 　　┗┓┓┏━┳┓┏┛
 * 　　　┃┫┫　┃┫┫
 * 　　　┗┻┛　┗┻┛
 *
 * @Description : 日志MQ发送工具
 * ---------------------------------
 * @Author : cyq
 * @Date : Create in 2018/11/20 09:53
 */
@Component
@RefreshScope
public class SendLog {
    // 消息发送者
    @Autowired
    private AmqpTemplate amqpTemplate;

    @Value("${opencloud.log.systemLog}")
    private Boolean sendSystemlog;
    /**
     *
     * 功能描述:
     *  用户ID，时间，，用户IP
     * @param: 功能名，功能描述，功能类型，切入点，参数，返回，访问耗时
     * @return:
     * @auther: cyq
     * @date: 2018/8/24 15:20
     */
    public void send(String module, String desc, String type, String joinPoint
            , String params, String returns, long totalTime){
        if(sendSystemlog==null || sendSystemlog) {
            SystemLog systemLog = new SystemLog();

            Principal p = HttpKit.getRequest().getUserPrincipal();
            String username = p.getName();
            if(username!=null) {
                systemLog.setUserId(username);
            } else {
                systemLog.setUserId("system");
            }

            systemLog.setCreateTime(new Timestamp(System.currentTimeMillis()));
            systemLog.setReqIp(HttpKit.getIpAddr());
            systemLog.setName(module);
            systemLog.setRemark(desc);
            systemLog.setType(type);
            systemLog.setJointPt(joinPoint);
            systemLog.setParams(params);
            systemLog.setResults(returns);
            systemLog.setTotalTime(totalTime);

            amqpTemplate.convertAndSend(QueueConstants.QUEUE_SYSTEM_CUSTOM_LOGS, systemLog);
        }
    }
}

