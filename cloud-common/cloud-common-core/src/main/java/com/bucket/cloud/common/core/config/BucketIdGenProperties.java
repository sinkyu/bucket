package com.bucket.cloud.common.core.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @auther: cyq
 * @Description: 自定义ID生成器配置
 * @Date: 2019/9/3 0:41
 */
@ConfigurationProperties(prefix = "bucket.id")
public class BucketIdGenProperties {
    /**
     * 工作ID (0~31)
     */
    private long workId = 0;
    /**
     * 数据中心ID (0~31)
     */
    private long centerId = 0;

    public long getWorkId() {
        return workId;
    }

    public void setWorkId(long workId) {
        this.workId = workId;
    }

    public long getCenterId() {
        return centerId;
    }

    public void setCenterId(long centerId) {
        this.centerId = centerId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("BucketIdGenProperties{");
        sb.append("workId=").append(workId);
        sb.append(", centerId=").append(centerId);
        sb.append('}');
        return sb.toString();
    }
}
