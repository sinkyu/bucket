package com.bucket.cloud.common.core.util;

import com.bucket.cloud.common.core.model.JsonTreeData;

import java.util.ArrayList;
import java.util.List;

public class TreeNodeUtil {

    public final static List<JsonTreeData> getfatherNode(List<JsonTreeData> treeDataList) {
        return getfatherNode(treeDataList, "0");
    }

    public final static List<JsonTreeData> getfatherNode(List<JsonTreeData> treeDataList, String parentCode) {
        List<JsonTreeData> newTreeDataList = new ArrayList<JsonTreeData>();
        for (JsonTreeData jsonTreeData : treeDataList) {
            if(org.apache.commons.lang3.StringUtils.isBlank(jsonTreeData.getPid())
                    || jsonTreeData.getPid() .equals(parentCode)) {
                //获取父节点下的子节点
                jsonTreeData.setChildren(getChildrenNode(jsonTreeData.getId(),treeDataList));
                jsonTreeData.setState("open");
                newTreeDataList.add(jsonTreeData);
            }
        }
        return newTreeDataList;
    }

    private final static List<JsonTreeData> getChildrenNode(String pid , List<JsonTreeData> treeDataList) {
        List<JsonTreeData> newTreeDataList = new ArrayList<JsonTreeData>();
        for (JsonTreeData jsonTreeData : treeDataList) {
            if(org.apache.commons.lang3.StringUtils.isBlank(jsonTreeData.getPid()))  continue;
            //这是一个子节点
            if(jsonTreeData.getPid().equals(pid)){
                //递归获取子节点下的子节点
                jsonTreeData.setChildren(getChildrenNode(jsonTreeData.getId() , treeDataList));
                newTreeDataList.add(jsonTreeData);
            }
        }
        return newTreeDataList;
    }
}