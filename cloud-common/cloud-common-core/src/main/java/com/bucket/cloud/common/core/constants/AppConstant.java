package com.bucket.cloud.common.core.constants;

/**
 * @Auther: cyq
 * @Date: 2019/8/31 22:31
 * @Description: 系统配置常量
 */
public interface AppConstant {

    String APPLICATION_SYSTEM_NAME = "bucket-system";
}
