package com.bucket.cloud.common.core.constants;

/**
 * @author: cyq
 * @date: 2019/2/21 17:46
 * @description:
 */
public class QueueConstants {

    public static final String QUEUE_SCAN_API_RESOURCE = "bucket.scan.api.resource";

    public static final String QUEUE_ACCESS_LOGS = "bucket.access.logs";

    public static final String QUEUE_SYSTEM_CUSTOM_LOGS = "bucket.system.custom.logs";
}
