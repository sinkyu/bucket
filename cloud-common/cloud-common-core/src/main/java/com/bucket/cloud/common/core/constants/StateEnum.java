package com.bucket.cloud.common.core.constants;

/**
 * @auther: cyq
 * @Description: 是否是启用的状态枚举
 * @Date: 2020/2/17 17:08
 */
public enum StateEnum {

    ENABLE(1, "启用"),
    DISENABLE(0, "禁用");//不是菜单的是按钮

    int code;
    String message;

    StateEnum(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static String valueOf(Integer status) {
        if (status == null) {
            return "";
        } else {
            for (StateEnum s : StateEnum.values()) {
                if (s.getCode() == status) {
                    return s.getMessage();
                }
            }
            return "";
        }
    }
}
