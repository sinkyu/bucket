package com.bucket.cloud.log.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.log.api.model.GatewayAccessLogs;
import com.bucket.cloud.log.api.model.GatewayAccessLogsPage;

/**
 * 网关访问日志
 * @author cyq
 */
public interface GatewayAccessLogsService {

    /**
     * 分页查询
     *
     * @param gatewayAccessLogsPage
     * @return
     */
    IPage<GatewayAccessLogs> findListPage(GatewayAccessLogsPage gatewayAccessLogsPage);
}
