package com.bucket.cloud.log.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bucket.cloud.log.api.model.GatewayAccessLogs;
import com.bucket.cloud.log.api.model.GatewayAccessLogsPage;
import com.bucket.cloud.log.dao.GatewayLogsMapper;
import com.bucket.cloud.log.service.GatewayAccessLogsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * @author cyq
 */
@Slf4j
@Service
@Transactional(rollbackFor = Exception.class)
public class GatewayAccessLogsServiceImpl implements GatewayAccessLogsService {

    @Resource
    private GatewayLogsMapper gatewayLogsMapper;

    /**
     * 分页查询
     *
     * @param logsPage
     * @return
     */
    @Override
    public IPage<GatewayAccessLogs> findListPage(GatewayAccessLogsPage logsPage) {
        IPage<GatewayAccessLogs> page = new Page<GatewayAccessLogs>(logsPage.getPageNumber(), logsPage.getPageSize());
        return gatewayLogsMapper.queryByParams(page,logsPage);
    }
}
