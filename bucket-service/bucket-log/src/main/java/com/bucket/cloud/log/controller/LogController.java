package com.bucket.cloud.log.controller;

import com.bucket.cloud.common.core.controller.BaseController;
import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.log.api.model.LogPage;
import com.bucket.cloud.log.service.LogService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/system/log")
@Api(value = "日志管理")
public class LogController extends BaseController {

    @Autowired
    private LogService logService;

    @ApiOperation(value = "列表", httpMethod = "POST")
    @RequestMapping(value = "/list")
    public Result list(@RequestBody LogPage logPage) {
        return Result.ok().data(logService.queryPage(logPage));
    }
}
