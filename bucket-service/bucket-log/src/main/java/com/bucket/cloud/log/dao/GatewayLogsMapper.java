package com.bucket.cloud.log.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.log.api.model.GatewayAccessLogs;
import com.bucket.cloud.log.api.model.GatewayAccessLogsPage;
import org.apache.ibatis.annotations.Param;

public interface GatewayLogsMapper extends BaseMapper<GatewayAccessLogs> {

    /**
     * @Author cyq
     * @Description 分页查询网关日志
     * @Date 2020/7/22 12:31 上午
    **/
    IPage<GatewayAccessLogs> queryByParams(IPage<GatewayAccessLogs> page, @Param("ew") GatewayAccessLogsPage log);
}