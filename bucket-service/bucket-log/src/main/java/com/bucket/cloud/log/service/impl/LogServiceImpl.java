package com.bucket.cloud.log.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bucket.cloud.log.api.model.LogPage;
import com.bucket.cloud.common.core.model.SystemLog;
import com.bucket.cloud.log.dao.LogMapper;
import com.bucket.cloud.log.service.LogService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * code is far away from bug with the animal protecting
 * ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃代码无BUG！
 * 　　┃　　　┗━━━┓
 * 　　┃　　　　　　　┣┓
 * 　　┃　　　　　　　┏┛
 * 　　┗┓┓┏━┳┓┏┛
 * 　　　┃┫┫　┃┫┫
 * 　　　┗┻┛　┗┻┛
 *
 * @Description :
 * ---------------------------------
 * @Author : cyq
 * @Date : Create in 2018/11/20 14:00
 */
@Service
@Transactional
public class LogServiceImpl implements LogService {

    @Resource
    private LogMapper logMapper;

    @Override
    public void addNewLog(SystemLog log) {
//        log.setCreateTime(new Timestamp(new Date().getTime()));
        logMapper.insert(log);
    }

    @Override
    public IPage<SystemLog> queryPage(LogPage logPage) {
        IPage<SystemLog> page = new Page<>(logPage.getPageNumber(), logPage.getPageSize());
        return logMapper.queryByParams(page, logPage);
    }
}
