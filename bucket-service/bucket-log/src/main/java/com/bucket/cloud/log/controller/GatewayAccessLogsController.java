package com.bucket.cloud.log.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.log.api.model.GatewayAccessLogs;
import com.bucket.cloud.log.api.model.GatewayAccessLogsPage;
import com.bucket.cloud.log.service.GatewayAccessLogsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 网关智能路由
 *
 * @author: cyq
 * @date: 2019/3/12 15:12
 * @description:
 */
@Api(tags = "网关访问日志")
@RestController
public class GatewayAccessLogsController {

    @Autowired
    private GatewayAccessLogsService gatewayAccessLogsService;

    /**
     * 获取分页列表
     *
     * @return
     */
    @ApiOperation(value = "获取分页访问日志列表", notes = "获取分页访问日志列表")
    @PostMapping("/gateway/access/logs")
    public Result<IPage<GatewayAccessLogs>> getAccessLogListPage(@RequestBody GatewayAccessLogsPage gatewayAccessLogsPage) {
        return Result.ok().data(gatewayAccessLogsService.findListPage(gatewayAccessLogsPage));
    }

}
