package com.bucket.cloud.log.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.log.api.model.LogPage;
import com.bucket.cloud.common.core.model.SystemLog;
import org.apache.ibatis.annotations.Param;

public interface LogMapper extends BaseMapper<SystemLog> {

    /**
     * @Author cyq
     * @Description 系统日志分页
     * @Date 2020/7/22 12:31 上午
    **/
    IPage<SystemLog> queryByParams(IPage<SystemLog> page, @Param("ew") LogPage log);
}
