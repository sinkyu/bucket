package com.bucket.cloud.log;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 日志管理服务
 * 提供系统日志管理、路由日志管理
 *
 * @author cyq
 */
@EnableCaching
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan(basePackages = "com.bucket.cloud.log.dao")
public class BucketLogApplication {

    public static void main(String[] args) {
        SpringApplication.run(BucketLogApplication.class, args);
        System.err.println("==============日志管理服务启动成功==============");
    }

}