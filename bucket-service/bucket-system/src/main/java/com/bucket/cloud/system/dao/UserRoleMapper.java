package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bucket.cloud.system.api.model.SystemUserRole;

/**
 * 用户角色关联表
 */
public interface UserRoleMapper extends BaseMapper<SystemUserRole> {
}