package com.bucket.cloud.system.controller;

import com.bucket.cloud.common.core.annotation.CustomLog;
import com.bucket.cloud.common.core.controller.BaseController;
import com.bucket.cloud.common.core.model.LogType;
import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.system.api.model.OrgPage;
import com.bucket.cloud.system.api.model.SystemOrg;
import com.bucket.cloud.system.service.OrgService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "机构管理")
@RestController
@RequestMapping("/system/org")
public class OrgController extends BaseController {

    @Autowired
    private OrgService orgService;

    @ApiOperation(value = "列表", httpMethod = "POST")
    @RequestMapping(value = "/list")
    @CustomLog(name = "查询机构", description = "查询机构列表", type = LogType.QUERY)
    public Result list(@RequestBody OrgPage org) {
        return Result.ok().data(orgService.queryPage(org));
    }

    @ApiOperation(value = "查全部", httpMethod = "POST")
    @RequestMapping(value = "/all")
    public Result all(@RequestBody SystemOrg org) {
        return Result.ok().data(orgService.queryAll(org));
    }

    @ApiOperation(value = "增加", httpMethod = "POST")
    @RequestMapping(value = "/add")
    @CustomLog(name = "新增机构", description = "新增机构", type = LogType.ADD)
    public Result add(@RequestBody SystemOrg org) {
        orgService.add(org);
        return Result.ok();
    }

    @ApiOperation(value = "更新", httpMethod = "POST")
    @RequestMapping(value = "/update")
    @CustomLog(name = "修改机构", description = "修改机构", type = LogType.EDIT)
    public Result update(@RequestBody SystemOrg org) {
        Assert.notNull(org.getId(), "必须参数不能为空！");
        orgService.edit(org);
        return Result.ok();

    }

    @ApiOperation(value = "删除", httpMethod = "POST")
    @RequestMapping(value = "/delete")
    @CustomLog(name = "删除机构", description = "删除了N个机构", type = LogType.DELETE)
    public Result delete(@RequestBody SystemOrg org) {
        orgService.del(org.getId().split(","));
        return Result.ok();

    }

    @ApiOperation(value = "启用", httpMethod = "POST")
    @RequestMapping(value = "/enable")
    @CustomLog(name = "启用机构", description = "启用了N个机构", type = LogType.EDIT)
    public Result enable(@RequestBody SystemOrg org) {
        orgService.enable(org.getId().split(","), 1);
        return Result.ok();
    }

    @ApiOperation(value = "禁用", httpMethod = "POST")
    @RequestMapping(value = "/disable")
    @CustomLog(name = "禁用机构", description = "禁用了N个机构", type = LogType.EDIT)
    public Result disable(@RequestBody SystemOrg org) {
        orgService.enable(org.getId().split(","), 0);
        return Result.ok();
    }


    @ApiOperation(value = "获取派出所字典", httpMethod = "POST")
    @RequestMapping(value = "/get")
    public Result get(@RequestBody SystemOrg org) {
        return Result.ok().data(orgService.get(org));
    }
}
