package com.bucket.cloud.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.system.api.model.RolePage;
import com.bucket.cloud.system.api.model.SystemRole;
import com.bucket.cloud.system.api.model.SystemRoleAddModel;

import java.util.List;

/**
 * 角色管理
 *
 * @author cyq
 */
public interface RoleService {

    /**
     * 新增角色
     *
     * @param role
     * @return
     */
    void insertRole(SystemRoleAddModel role);

    /**
     * 修改角色
     *
     * @param role
     * @return
     */
    void updateRole(SystemRoleAddModel role);

    /**
     * 删除角色
     *
     * @param id 角色集合id
     * @return
     */
    void deleteRole(String[] id);

    /**
     * 分页查询角色
     *
     * @param role
     * @return
     */
    IPage<SystemRole> findTByPage(RolePage role);

    /**
     * 获取所有角色
     *
     * @return
     */
    List<SystemRole> selectAll();

    /**
     * 启用/禁用角色
     *
     * @param id 角色id集合
     * @param state 1:启用，0禁用
     * @return
     */
    void enable(String[] id, int state);

}
