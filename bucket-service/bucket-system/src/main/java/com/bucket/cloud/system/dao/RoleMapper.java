package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.system.api.model.RolePage;
import com.bucket.cloud.system.api.model.SystemRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 角色管理
 */
public interface RoleMapper extends BaseMapper<SystemRole> {

    IPage<SystemRole> queryByParams(IPage<SystemRole> page, @Param("ew") RolePage rolePage);

    List<SystemRole> queryUserRoles(@Param("userId") String userId);
}