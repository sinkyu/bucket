package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.system.api.model.SystemGatewayIplimit;
import com.bucket.cloud.system.api.model.SystemGatewayIplimitPage;
import com.bucket.cloud.system.api.vo.SystemGatewayIplimitVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 白名单管理
 */
public interface GatewayIplimitMapper extends BaseMapper<SystemGatewayIplimit> {

    List<SystemGatewayIplimitVO> queryIplimitList(SystemGatewayIplimitVO params);

    IPage<SystemGatewayIplimit> queryApiList(IPage page, @Param("ew") SystemGatewayIplimitPage params);

    SystemGatewayIplimitVO queryInfo(SystemGatewayIplimit params);
}
