package com.bucket.cloud.system.service;

import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.system.api.model.SystemGatewayIplimit;
import com.bucket.cloud.system.api.model.SystemGatewayIplimitPage;
import com.bucket.cloud.system.api.vo.SystemGatewayIplimitVO;
import com.bucket.cloud.system.api.vo.SystemInterfaceVO;

import java.util.List;

public interface GatewayIplimitService {

    Result<List<SystemInterfaceVO>> queryLimitApiList(SystemGatewayIplimitVO params);

    Object findTByPage(SystemGatewayIplimitPage params);

    Object selectByParams(SystemGatewayIplimit params);

    void insert(SystemGatewayIplimitVO params);

    void update(SystemGatewayIplimitVO params);

    void delete(String[] split);

    void enable(String[] split, int i);

    Object queryInfo(SystemGatewayIplimit params);
}
