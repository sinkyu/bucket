package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bucket.cloud.system.api.model.SystemUserPosition;

/**
 * 用户职位关联表
 */
public interface UserPositionMapper extends BaseMapper<SystemUserPosition> {
}
