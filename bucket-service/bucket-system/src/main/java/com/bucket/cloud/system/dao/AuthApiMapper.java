package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bucket.cloud.system.api.model.SystemAuthApi;

/**
 * 授权api关联
 */
public interface AuthApiMapper extends BaseMapper<SystemAuthApi> {
}
