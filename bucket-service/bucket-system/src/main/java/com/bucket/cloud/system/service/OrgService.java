package com.bucket.cloud.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.common.core.model.JsonTreeData;
import com.bucket.cloud.system.api.model.OrgPage;
import com.bucket.cloud.system.api.model.SystemOrg;

import java.util.List;

/**
 * 组织机构管理
 *
 * @author cyq
 */
public interface OrgService {

    void add(SystemOrg org);

    void del(String[] id);

    void edit(SystemOrg org);

    void enable(String[] id, int state);

    IPage<SystemOrg> queryPage(OrgPage orgPage);

    List<JsonTreeData> queryAll(SystemOrg org);

    List<SystemOrg> get(SystemOrg org);
}
