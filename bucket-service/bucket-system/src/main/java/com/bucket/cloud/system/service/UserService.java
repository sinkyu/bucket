package com.bucket.cloud.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bucket.cloud.system.api.model.SystemResources;
import com.bucket.cloud.system.api.model.SystemUser;
import com.bucket.cloud.system.api.model.SystemUserAddModel;
import com.bucket.cloud.system.api.model.UserPage;
import com.bucket.cloud.system.api.vo.SystemUserVO;

import java.util.List;

/**
 * 用户管理
 *
 * @author cyq
 */
public interface UserService extends IService<SystemUser> {

    /**
     * 新增用户
     *
     * @param user
     * @return
     */
    void insertUser(SystemUserAddModel user);

    /**
     * 更新用户
     *
     * @param user
     * @return
     */
    void updateUser(SystemUserAddModel user);

    /**
     * 删除用户
     *
     * @param id 用户id集合
     * @return
     */
    void deleteUser(String[] id);

    /**
     * 更新用户密码
     *
     * @param user
     * @return
     */
    void updatePassword(SystemUser user);

    /**
     * 分页查询用户
     *
     * @param userPage
     * @return
     */
    IPage<SystemUser> findTByPage(UserPage userPage);

    /**
     * 查询用户列表
     *
     * @param user
     * @return
     */
    List<SystemUser> selectByParams(SystemUser user);

    /**
     * 启用/禁用用户
     *
     * @param id 用户id集合
     * @param state 1:启用，0禁用
     * @return
     */
    void enable(String[] id, int state);

    /**
     * 获取单个用户所有信息
     *
     * @param user
     * @return
     */
    SystemUser getThisUserInfo(SystemUser user);

}
