package com.bucket.cloud.system.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.common.core.annotation.CustomLog;
import com.bucket.cloud.common.core.model.LogType;
import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.common.core.security.OpenClientDetails;
import com.bucket.cloud.common.core.security.http.OpenRestTemplate;
import com.bucket.cloud.system.api.model.SystemApp;
import com.bucket.cloud.system.api.model.SystemAppPage;
import com.bucket.cloud.system.api.service.ISystemAppServiceClient;
import com.bucket.cloud.system.service.SystemAppService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Api(tags = "系统应用管理")
@RestController
@RequestMapping("/system/app")
public class SystemAppController implements ISystemAppServiceClient {
    @Autowired
    private SystemAppService systemAppService;
    @Autowired
    private OpenRestTemplate openRestTemplate;

    /**
     * 获取分页应用列表
     *
     * @return
     */
    @ApiOperation(value = "获取分页应用列表", notes = "获取分页应用列表")
    @RequestMapping("/list")
    @CustomLog(name = "查询应用", description = "查询应用列表", type = LogType.QUERY)
    public Result<IPage<SystemApp>> getAppListPage(@RequestBody SystemAppPage systemAppPage) {
        IPage<SystemApp> IPage = systemAppService.findListPage(systemAppPage);
        return Result.ok().data(IPage);
    }

    /**
     * @auther: cyq
     * @Description: 添加应用信息
     * @Date: 2020/6/12 17:47
     */
    @ApiOperation(value = "添加应用信息", notes = "添加应用信息")
    @RequestMapping("/add")
    @CustomLog(name = "新增应用", description = "新增应用", type = LogType.ADD)
    public Result add(@RequestBody SystemApp systemApp) {
        systemApp = systemAppService.addApp(systemApp);
        systemAppService.addClient(systemApp);
        return Result.ok();
    }

    /**
     * @auther: cyq
     * @Description: 修改应用信息
     * @Date: 2020/6/12 17:47
     */
    @ApiOperation(value = "修改应用信息", notes = "修改应用信息")
    @RequestMapping("/update")
    @CustomLog(name = "修改应用", description = "修改应用", type = LogType.EDIT)
    public Result update(@RequestBody SystemApp systemApp) {
        systemApp = systemAppService.updateApp(systemApp);
        systemAppService.updateClient(systemApp);
        return Result.ok();
    }

    /**
     * 获取应用详情
     *
     * @param aid
     * @return
     */
    @ApiOperation(value = "获取应用详情", notes = "获取应用详情")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "aid", value = "应用ID", defaultValue = "1", required = true, paramType = "path"),
    })
    @GetMapping("/{aid}/info")
    @Override
    public Result<SystemApp> getApp(@PathVariable("aid") String aid) {
        SystemApp appInfo = systemAppService.getAppInfo(aid);
        return Result.ok().data(appInfo);
    }

    /**
     * 获取应用开发配置信息
     *
     * @param clientId
     * @return
     */
    @ApiOperation(value = "获取应用开发配置信息", notes = "获取应用开发配置信息")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "clientId", value = "应用ID", defaultValue = "1", required = true, paramType = "path"),
    })
    @GetMapping("/client/{clientId}/info")
    @Override
    public Result<OpenClientDetails> getAppClientInfo(
            @PathVariable("clientId") String clientId
    ) {
        OpenClientDetails clientInfo = systemAppService.getAppClientInfo(clientId);
        return Result.ok().data(clientInfo);
    }

    /**
     * 重置应用秘钥
     *
     * @param aid 应用Id
     * @return
     */
    @ApiOperation(value = "重置应用秘钥", notes = "重置应用秘钥")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "aid", value = "应用Id", required = true, paramType = "form"),
    })
    @PostMapping("/reset")
    public Result<String> resetAppSecret(
            @RequestParam("aid") String aid
    ) {
        String result = systemAppService.restSecret(aid);
        return Result.ok().data(result);
    }

    /**
     * 删除应用信息
     *
     * @return
     */
    @ApiOperation(value = "删除应用信息", notes = "删除应用信息")
    @PostMapping("/delete")
    @CustomLog(name = "删除应用", description = "删除应用", type = LogType.DELETE)
    public Result delete(@RequestBody SystemApp app) {
        String[] ids = app.getAppId().split(",");
        for (String appId : ids) {
            systemAppService.removeApp(appId);
        }
        openRestTemplate.refreshGateway();
        return Result.ok();
    }

    @ApiOperation(value = "禁用应用", notes = "根据id，批量禁用应用", httpMethod = "POST")
    @RequestMapping(value = "/disable")
    @CustomLog(name = "禁用应用", description = "禁用应用", type = LogType.EDIT)
    public Result disable(@RequestBody SystemApp app) {
        systemAppService.enable(app.getAppId().split(","), 0);
        return Result.ok();
    }

    @ApiOperation(value = "启用应用", notes = "根据id，批量启用应用", httpMethod = "POST")
    @RequestMapping(value = "/enable")
    @CustomLog(name = "启用应用", description = "启用应用", type = LogType.EDIT)
    public Result enable(@RequestBody SystemApp app) {
        systemAppService.enable(app.getAppId().split(","), 1);
        return Result.ok();
    }
}
