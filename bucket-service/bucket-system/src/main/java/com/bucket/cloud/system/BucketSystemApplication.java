package com.bucket.cloud.system;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;

/**
 * 平台基础服务
 * 提供系统用户、权限分配、资源、客户端管理
 *
 * @author cyq
 */
@EnableCaching
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@ComponentScan(basePackages = {"com.bucket.cloud"})
@MapperScan(basePackages = "com.bucket.cloud.system.dao")
public class BucketSystemApplication {

    public static void main(String[] args) {
        SpringApplication.run(BucketSystemApplication.class, args);
        System.err.println("==============系统管理启动成功==============");
    }

}