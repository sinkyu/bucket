package com.bucket.cloud.system.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.system.api.model.SystemApp;
import com.bucket.cloud.system.api.model.SystemAppPage;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

/**
 * 应用管理
 */
public interface SystemAppMapper extends BaseMapper<SystemApp> {

    IPage<SystemApp> queryByParams(IPage<SystemApp> page, @Param("ew") SystemAppPage systemAppPage);
}
