package com.bucket.cloud.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bucket.cloud.system.api.model.RolePage;
import com.bucket.cloud.system.api.model.SystemRole;
import com.bucket.cloud.system.api.model.SystemRoleAddModel;
import com.bucket.cloud.system.api.model.SystemRoleResources;
import com.bucket.cloud.system.dao.RoleMapper;
import com.bucket.cloud.system.dao.RoleResourcesMapper;
import com.bucket.cloud.system.service.RoleService;
import com.bucket.cloud.system.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;


@Service
@Transactional
public class RoleServiceImpl implements RoleService {

    @Resource
    private RoleMapper roleMapper;
    @Autowired
    private UserService userService;
    @Resource
    private RoleResourcesMapper roleResourcesMapper;


    @Override
    public void insertRole(SystemRoleAddModel role) {
        role.setId(UUID.randomUUID().toString().replace("-", ""));
        roleMapper.insert(role.getSystemRole());
        for (String id:role.getResourcesId()) {
            SystemRoleResources sr = new SystemRoleResources();
            sr.setRoleId(role.getId());
            sr.setResourcesId(id);
            roleResourcesMapper.insert(sr);
        }
    }

    @Override
    public void updateRole(SystemRoleAddModel role) {
        roleMapper.updateById(role.getSystemRole());
        roleResourcesMapper.delete(new QueryWrapper<SystemRoleResources>()
                .eq("role_id", role.getId()));
        for (String id:role.getResourcesId()) {
            SystemRoleResources sr = new SystemRoleResources();
            sr.setRoleId(role.getId());
            sr.setResourcesId(id);
            roleResourcesMapper.insert(sr);
//            ShiroUtil.clearAuth();
        }
    }

    @Override
    public void deleteRole(String[] id) {
        roleResourcesMapper.delete(new QueryWrapper<SystemRoleResources>().in("role_id", id));
        roleMapper.deleteBatchIds(Arrays.asList(id));
    }

    @Override
    public IPage<SystemRole> findTByPage(RolePage role) {
        IPage<SystemRole> pageObj = new Page(role.getPageNumber(), role.getPageSize());
        return roleMapper.queryByParams(pageObj, role);
    }

    @Override
    public List<SystemRole> selectAll() {
        return roleMapper.selectList(null);
    }

    @Override
    public void enable(String[] id, int state) {
        for(String i : id){
            SystemRole rs = new SystemRole();
            rs.setId(i);
            rs.setState(state);
            roleMapper.updateById(rs);
        }
    }
}
