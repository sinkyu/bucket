package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bucket.cloud.system.api.model.GatewayAccessLogs;
import org.springframework.stereotype.Repository;

/**
 * 网关日志管理
 */
@Repository
public interface GatewayLogsMapper extends BaseMapper<GatewayAccessLogs> {
}
