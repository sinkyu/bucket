package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bucket.cloud.system.api.model.SystemUserResources;

/**
 * 用户菜单关联表
 */
public interface UserResourcesMapper extends BaseMapper<SystemUserResources> {
}
