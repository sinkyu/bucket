package com.bucket.cloud.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.system.api.model.SystemAuthApi;
import com.bucket.cloud.system.api.model.SystemInterface;
import com.bucket.cloud.system.api.model.SystemInterfacePage;
import com.bucket.cloud.system.api.model.SystemUser;
import com.bucket.cloud.system.api.vo.SystemInterfaceVO;

import java.util.List;

/**
 * Api管理
 *
 * @author cyq
 */
public interface GatewayInterfaceService extends IService<SystemInterface> {

    Result<List<SystemInterfaceVO>> queryApiAllList(SystemInterfaceVO params);

    Result<List<SystemInterface>> queryAllApi();

    void addApiUseMerge(SystemInterface params);

    List<SystemInterface> queryUserApi(SystemUser params);

    Result<List<SystemInterfaceVO>> queryApiList(SystemInterfaceVO params);

    IPage<SystemInterfaceVO> findByPage(SystemInterfacePage params);

    Result bindApi(SystemAuthApi params);

    Result unbindAllApi(SystemAuthApi params);

    Result addApi(SystemInterface params);

    Result delApi(String[] ids);

    Result updateApi(SystemInterface params);

    Result enableApi(String[] ids, int enable);
}
