package com.bucket.cloud.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bucket.cloud.common.core.security.OpenClientDetails;
import com.bucket.cloud.system.api.model.SystemApp;
import com.bucket.cloud.system.api.model.SystemAppPage;

/**
 * 应用信息管理
 *
 * @author cyq
 */
public interface SystemAppService extends IService<SystemApp> {

    /**
     * 查询应用列表
     *
     * @param systemAppPage
     * @return
     */
    IPage<SystemApp> findListPage(SystemAppPage systemAppPage);

    /**
     * 添加应用
     *
     * @param app
     * @return
     */
    SystemApp addApp(SystemApp app);

    /**
     * 修改应用
     *
     * @param app
     * @return
     */
    SystemApp updateApp(SystemApp app);

    /**
     * 添加应用信息
     *
     * @param app
     * @return
     */
    OpenClientDetails addClient(SystemApp app);

    /**
     * 修改应用信息
     *
     * @param app
     * @return
     */
    OpenClientDetails updateClient(SystemApp app);

    /**
     * 获取app信息
     *
     * @param appId
     * @return
     */
    SystemApp getAppInfo(String appId);

    /**
     * 获取app和应用信息
     *
     * @param clientId
     * @return
     */
    OpenClientDetails getAppClientInfo(String clientId);

    /**
     * 重置秘钥
     *
     * @param appId
     * @return
     */
    String restSecret(String appId);

    /**
     * 删除应用
     *
     * @param appId
     * @return
     */
    void removeApp(String appId);

    /**
     * 删除应用
     *
     * @param ids
     * @return
     */
    void enable(String[] ids, int status);
}
