package com.bucket.cloud.system.controller;

import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.system.api.model.SystemGatewayRouter;
import com.bucket.cloud.system.api.model.SystemGatewayRouterPage;
import com.bucket.cloud.system.service.GatewayRouterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "网关对外接口")
@RestController
@RequestMapping("/gateway")
public class GatewayController {

    @Autowired
    GatewayRouterService gatewayRouterService;

//    @Autowired
//    OpenRestTemplate restTemplate;

//    @ApiIgnore
//    @RequestMapping(value = "/refresh")
//    public Object refresh() {
//        restTemplate.refreshGateway();
//        return ResultBody.success();
//    }

    @ApiOperation(value = "分页查询路由", notes = "根据请求的参数分页查询路由列表")
    @RequestMapping(value = "/router/page")
    public Object routPage(@RequestBody SystemGatewayRouterPage params) {
        return Result.ok().data(gatewayRouterService.findTByPage(params));
    }

    @ApiOperation(value = "查询路由列表", notes = "根据参数查询所有路由列表")
    @RequestMapping(value = "/router/list")
    public Object routList(@RequestBody SystemGatewayRouter params) {
        return Result.ok().data(gatewayRouterService.selectByParams(params));
    }

    @ApiOperation(value = "新增路由", notes = "添加一个新的路由信息")
    @RequestMapping(value = "/router/add")
    public Object routAdd(@RequestBody SystemGatewayRouter params) {
        gatewayRouterService.insert(params);
        return Result.ok();
    }

    @ApiOperation(value = "修改路由", notes = "根据参数修改路由信息")
    @RequestMapping(value = "/router/update")
    public Object routUpdate(@RequestBody SystemGatewayRouter params) {
        gatewayRouterService.update(params);
        return Result.ok();
    }

    @ApiOperation(value = "删除路由", notes = "根据提供的id，批量删除路由")
    @RequestMapping(value = "/router/del")
    public Object routDel(@RequestBody SystemGatewayRouter params) {
        gatewayRouterService.delete(params.getId().split(","));
        return Result.ok();
    }

    @ApiOperation(value = "启用路由", notes = "根据提供的id，批量启用路由")
    @RequestMapping(value = "/router/enable")
    public Object routEnable(@RequestBody SystemGatewayRouter params) {
        gatewayRouterService.enable(params.getId().split(","), 1);
        return Result.ok();
    }

    @ApiOperation(value = "禁用路由", notes = "根据提供的id，批量禁用路由")
    @RequestMapping(value = "/router/disable")
    public Object routDisable(@RequestBody SystemGatewayRouter params) {
        gatewayRouterService.enable(params.getId().split(","), 0);
        return Result.ok();
    }
}
