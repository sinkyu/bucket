package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.system.api.model.SystemUser;
import com.bucket.cloud.system.api.model.UserPage;
import com.bucket.cloud.system.api.vo.SystemUserVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 用户管理
 */
public interface UserMapper extends BaseMapper<SystemUser> {

    /**
     * @Author cyq
     * @Description 分页查询
     * @Date 2020/8/14 6:43 下午
    **/
    IPage<SystemUser> queryByParams(IPage<SystemUser> page, @Param("ew") UserPage user);

    /**
     * @Author cyq
     * @Description 获取用户列表
     * @Date 2020/8/14 6:43 下午
     **/
    List<SystemUser> queryByParams(@Param("ew") SystemUser user);

}