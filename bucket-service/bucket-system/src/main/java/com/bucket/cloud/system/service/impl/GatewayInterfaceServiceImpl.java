package com.bucket.cloud.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.system.api.model.SystemAuthApi;
import com.bucket.cloud.system.api.model.SystemInterface;
import com.bucket.cloud.system.api.model.SystemInterfacePage;
import com.bucket.cloud.system.api.model.SystemUser;
import com.bucket.cloud.system.api.vo.SystemInterfaceVO;
import com.bucket.cloud.system.dao.AuthApiMapper;
import com.bucket.cloud.system.dao.GatewayInterfaceMapper;
import com.bucket.cloud.system.service.GatewayInterfaceService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.*;

@Service
public class GatewayInterfaceServiceImpl extends ServiceImpl<GatewayInterfaceMapper, SystemInterface> implements GatewayInterfaceService {

    @Resource
    GatewayInterfaceMapper gatewayInterfaceMapper;
    @Resource
    AuthApiMapper authApiMapper;

    @Override
    public Result<List<SystemInterfaceVO>> queryApiAllList(SystemInterfaceVO params) {
        List<SystemInterfaceVO> list = gatewayInterfaceMapper.queryApiList(params);
        return Result.ok().data(list);
    }

    @Override
    public Result<List<SystemInterface>> queryAllApi() {
        List<SystemInterface> list = gatewayInterfaceMapper.queryAllApi();
        return Result.ok().data(list);
    }

    @Override
    public void addApiUseMerge(SystemInterface params) {
        SystemInterface si = gatewayInterfaceMapper.selectById(params);
        if (si == null) {
            // 新增
            params.setCreateTime(Calendar.getInstance().getTime());
            gatewayInterfaceMapper.insert(params);
        } else {
            // 修改
            SystemInterface ins = new SystemInterface();
            ins.setId(params.getId());
            ins.setUpdateTime(Calendar.getInstance().getTime());
            ins.setIntName(params.getIntName());
            ins.setIntUrl(params.getIntUrl());
            ins.setRemark(params.getRemark());
            gatewayInterfaceMapper.updateById(ins);
        }
//        gatewayInterfaceMapper.mergeApi(params);
    }

    @Override
    public List<SystemInterface> queryUserApi(SystemUser params) {
        List<SystemInterface> list = gatewayInterfaceMapper.queryUserApi(params);
        return list;
    }

    @Override
    public Result<List<SystemInterfaceVO>> queryApiList(SystemInterfaceVO params) {
        List list = gatewayInterfaceMapper.queryApiListForParams(params);
        return Result.ok().data(list);
    }

    @Override
    public IPage<SystemInterfaceVO> findByPage(SystemInterfacePage params) {
        IPage pageObj = new Page(params.getPageNumber(), params.getPageSize());
        pageObj.setRecords(gatewayInterfaceMapper.queryApiList(pageObj, params));
        return pageObj;
    }

    @Override
    public Result bindApi(SystemAuthApi params) {
        // 先解除之前的绑定
        this.unbindAllApi(params);
        // 重新绑定
        for (String intId : params.getIntId().split(",")) {
            SystemAuthApi d = new SystemAuthApi();
            d.setId(UUID.randomUUID().toString().replace("-", ""));
            d.setAuthId(params.getAuthId());
            d.setAuthType(params.getAuthType());
            d.setIntId(intId);

            authApiMapper.insert(d);
        }
        return Result.ok();
    }

    @Override
    public Result unbindAllApi(SystemAuthApi params) {
        Map<String, Object> pm = new HashMap<>();
        pm.put("AUTH_ID", params.getAuthId());
        pm.put("AUTH_TYPE", params.getAuthType());
        authApiMapper.deleteByMap(pm);
        return Result.ok();
    }

    @Override
    public Result addApi(SystemInterface params) {
        int i = gatewayInterfaceMapper.insert(params);
        return i > 0 ? Result.ok() : Result.failed();
    }

    @Override
    public Result delApi(String[] ids) {
        int i = gatewayInterfaceMapper.deleteBatchIds(Arrays.asList(ids));
        return i > 0 ? Result.ok() : Result.failed();
    }

    @Override
    public Result updateApi(SystemInterface params) {
        params.setUpdateTime(new Date());
        int i = gatewayInterfaceMapper.updateById(params);
        return i > 0 ? Result.ok() : Result.failed();
    }

    @Override
    public Result enableApi(String[] ids, int enable) {
        for (String i : ids) {
            SystemInterface rs = new SystemInterface();
            rs.setId(i);
            rs.setState(enable);
            rs.setUpdateTime(new Date());
            gatewayInterfaceMapper.updateById(rs);
        }
        return Result.ok();
    }
}
