package com.bucket.cloud.system.controller;

import com.bucket.cloud.common.core.controller.BaseController;
import com.bucket.cloud.system.api.model.SystemGatewayRouter;
import com.bucket.cloud.system.api.service.IGatewayRouteServiceClient;
import com.bucket.cloud.system.service.GatewayRouterService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "路由管理")
@RestController
@RequestMapping("/gateway/api")
public class GatewayRouterController extends BaseController implements IGatewayRouteServiceClient {

    @Autowired
    GatewayRouterService gatewayRouterService;

    @PostMapping(value = "/getRouterList")
    @Override
    public List<SystemGatewayRouter> selectByParams(@RequestBody SystemGatewayRouter params) {
        return gatewayRouterService.selectByParams(params);
    }
}
