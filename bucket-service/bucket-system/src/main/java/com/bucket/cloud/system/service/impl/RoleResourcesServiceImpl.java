package com.bucket.cloud.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bucket.cloud.system.api.model.SystemRoleResources;
import com.bucket.cloud.system.dao.RoleResourcesMapper;
import com.bucket.cloud.system.service.RoleResourcesService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;

/**
 * code is far away from bug with the animal protecting
 * ┏┛ ┻━━━━━┛ ┻┓
 * ┃　　　　　　 ┃
 * ┃　　　━　　　┃
 * ┃　┳┛　  ┗┳　┃
 * ┃　　　　　　 ┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　 ┃
 * ┗━┓　　　┏━━━┛
 * ┃　　　┃   神兽保佑
 * ┃　　　┃   代码无BUG！
 * ┃　　　┗━━━━━━━━━┓
 * ┃　　　　　　　    ┣┓
 * ┃　　　　         ┏┛
 * ┗━┓ ┓ ┏━━━┳ ┓ ┏━┛
 * ┃ ┫ ┫   ┃ ┫ ┫
 * ┗━┻━┛   ┗━┻━┛
 *
 * @Description :
 * ---------------------------------
 * @Author : cyq
 * @Date : 2018/11/17 15:50
 */
@Service
@Transactional
public class RoleResourcesServiceImpl implements RoleResourcesService {

    @Resource
    private RoleResourcesMapper roleResourcesMapper;

    @Override
    public void deleteByRoleId(String[] id) {
        roleResourcesMapper.delete(new QueryWrapper<SystemRoleResources>().in("role_id", id));
    }

    @Override
    public void deleteByResourcesId(String[] id) {
        roleResourcesMapper.delete(new QueryWrapper<SystemRoleResources>().in("resources_id", id));
    }

    @Override
    public void add(String roleId, String[] resourcesIds) {
        for(String id:resourcesIds){
            SystemRoleResources rr = new SystemRoleResources();
            rr.setResourcesId(id);
            rr.setRoleId(roleId);
            roleResourcesMapper.insert(rr);
        }
    }
}
