package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.system.api.model.OrgPage;
import com.bucket.cloud.system.api.model.SystemOrg;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 组织机构管理
 */
public interface OrgMapper extends BaseMapper<SystemOrg> {

    IPage<SystemOrg> queryByParams(IPage<SystemOrg> page, @Param("ew") OrgPage orgPage);

    List<SystemOrg> queryTreeByParams(SystemOrg org);

}
