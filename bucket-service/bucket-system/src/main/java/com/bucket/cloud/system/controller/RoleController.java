package com.bucket.cloud.system.controller;


import com.bucket.cloud.common.core.annotation.CustomLog;
import com.bucket.cloud.common.core.controller.BaseController;
import com.bucket.cloud.common.core.model.LogType;
import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.system.api.model.RolePage;
import com.bucket.cloud.system.api.model.SystemRole;
import com.bucket.cloud.system.api.model.SystemRoleAddModel;
import com.bucket.cloud.system.service.RoleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "角色管理")
@RestController
@RequestMapping("/system/role")
public class RoleController extends BaseController {
    @Autowired
    private RoleService roleService;

    @ApiOperation(value = "列表", httpMethod = "POST")
    @RequestMapping(value = "/list")
    @CustomLog(name = "查询角色", description = "查询角色列表", type = LogType.QUERY)
    public Result list(@RequestBody RolePage role) {
        return Result.ok().data(roleService.findTByPage(role));
    }

    @ApiOperation(value = "增加", httpMethod = "POST")
    @RequestMapping(value = "/add")
    @CustomLog(name = "新增角色", description = "新增角色", type = LogType.ADD)
    public Result add(@RequestBody SystemRoleAddModel role) {
        roleService.insertRole(role);
        return Result.ok();

    }

    @ApiOperation(value = "删除", httpMethod = "POST")
    @RequestMapping(value = "/delete")
    @CustomLog(name = "删除角色", description = "删除角色", type = LogType.DELETE)
    public Result delete(@RequestBody SystemRole role) {
        roleService.deleteRole(role.getId().split(","));
        return Result.ok();
    }

    @ApiOperation(value = "更新", httpMethod = "POST")
    @RequestMapping(value = "/update")
    @CustomLog(name = "修改角色", description = "修改角色", type = LogType.EDIT)
    public Result update(@RequestBody SystemRoleAddModel mrole) {
        Assert.notNull(mrole.getId(), "必填参数不能为空");
        roleService.updateRole(mrole);
        return Result.ok();
    }

    @ApiOperation(value = "启用", httpMethod = "POST")
    @RequestMapping(value = "/enable")
    @CustomLog(name = "启用角色", description = "启用角色", type = LogType.EDIT)
    public Result enable(@RequestBody SystemRole systemRole) {
        roleService.enable(systemRole.getId().split(","), 1);
        return Result.ok();
    }

    @ApiOperation(value = "禁用", httpMethod = "POST")
    @RequestMapping(value = "/disable")
    @CustomLog(name = "禁用角色", description = "禁用角色", type = LogType.EDIT)
    public Result disable(@RequestBody SystemRole systemRole) {
        roleService.enable(systemRole.getId().split(","), 0);
        return Result.ok();
    }

    @ApiOperation(value = "查全部", httpMethod = "POST")
    @RequestMapping(value = "/all")
    public Result listAll() {
        return Result.ok().data(roleService.selectAll());
    }
}
