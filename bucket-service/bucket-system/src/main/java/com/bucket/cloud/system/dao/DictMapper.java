package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.system.api.model.DictPage;
import com.bucket.cloud.system.api.model.SystemDict;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 字典管理
 */
public interface DictMapper extends BaseMapper<SystemDict> {

    IPage<SystemDict> queryByParams(IPage<SystemDict> page, @Param("ew") DictPage dict);

    List<SystemDict> queryTreeByParams(SystemDict dict);
}