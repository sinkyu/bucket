package com.bucket.cloud.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.system.api.model.SystemGatewayIplimit;
import com.bucket.cloud.system.api.model.SystemGatewayIplimitPage;
import com.bucket.cloud.system.api.model.SystemIplimitInterface;
import com.bucket.cloud.system.api.vo.SystemGatewayIplimitVO;
import com.bucket.cloud.system.api.vo.SystemInterfaceVO;
import com.bucket.cloud.system.dao.GatewayInterfaceMapper;
import com.bucket.cloud.system.dao.GatewayIplimitMapper;
import com.bucket.cloud.system.dao.IplimitIntMapper;
import com.bucket.cloud.system.service.GatewayIplimitService;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class GatewayIplimitServiceImpl implements GatewayIplimitService {

    @Resource
    GatewayIplimitMapper gatewayIplimitMapper;

    @Resource
    GatewayInterfaceMapper gatewayInterfaceMapper;

    @Resource
    IplimitIntMapper iplimitIntMapper;

    @Override
    public Result<List<SystemInterfaceVO>> queryLimitApiList(SystemGatewayIplimitVO params) {
        SystemInterfaceVO newP = new SystemInterfaceVO();
        newP.setLimitType(params.getLimitType());
        newP.setIplimitState(params.getState());
        List<SystemInterfaceVO> list = gatewayInterfaceMapper.queryApiList(newP);
        return Result.ok().data(list);
    }

    @Override
    public Object findTByPage(SystemGatewayIplimitPage params) {
        IPage pageObj = new Page(params.getPageNumber(), params.getPageSize());
        return gatewayIplimitMapper.queryApiList(pageObj, params);
    }

    @Override
    public Object selectByParams(SystemGatewayIplimit params) {
        return null;
    }

    @Override
    public void insert(SystemGatewayIplimitVO params) {
        params.setId(UUID.randomUUID().toString().replaceAll("-", ""));
        gatewayIplimitMapper.insert(params);
        for (String id : params.getIntIds().split(",")) {
            SystemIplimitInterface sii = new SystemIplimitInterface();
            sii.setId(UUID.randomUUID().toString().replaceAll("-", ""));
            sii.setLimitId(params.getId());
            sii.setInterfaceId(id);
            iplimitIntMapper.insert(sii);
        }
    }

    @Override
    public void update(SystemGatewayIplimitVO params) {
        gatewayIplimitMapper.updateById(params);
        // 先删除之前绑定的接口
        iplimitIntMapper.delete(new QueryWrapper<SystemIplimitInterface>().eq("LIMIT_ID", params.getId()));
        // 重新绑定接口
        if (StringUtils.isNotBlank(params.getIntIds())) {
            for (String id : params.getIntIds().split(",")) {
                SystemIplimitInterface sii = new SystemIplimitInterface();
                sii.setId(UUID.randomUUID().toString().replaceAll("-", ""));
                sii.setLimitId(params.getId());
                sii.setInterfaceId(id);
                iplimitIntMapper.insert(sii);
            }
        }
    }

    @Override
    public void delete(String[] id) {
        gatewayIplimitMapper.deleteBatchIds(Arrays.asList(id));
        iplimitIntMapper.delete(new QueryWrapper<SystemIplimitInterface>().in("LIMIT_ID", Arrays.asList(id)));
    }

    @Override
    public void enable(String[] split, int i) {
        for (String id : split) {
            SystemGatewayIplimit rs = new SystemGatewayIplimit();
            rs.setId(id);
            rs.setState(i);
            gatewayIplimitMapper.updateById(rs);
        }
    }

    @Override
    public Object queryInfo(SystemGatewayIplimit params) {
        return gatewayIplimitMapper.queryInfo(params);
    }
}
