package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bucket.cloud.system.api.model.SystemRoleResources;

/**
 * 角色菜单关联表
 */
public interface RoleResourcesMapper extends BaseMapper<SystemRoleResources> {
}