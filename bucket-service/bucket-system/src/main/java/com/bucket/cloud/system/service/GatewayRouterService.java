package com.bucket.cloud.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.system.api.model.SystemGatewayRouter;
import com.bucket.cloud.system.api.model.SystemGatewayRouterPage;

import java.util.List;

/**
 * 路由管理
 *
 * @author cyq
 */
public interface GatewayRouterService {

    SystemGatewayRouter selectByPrimaryKey(String id);

    void insert(SystemGatewayRouter params);

    void update(SystemGatewayRouter params);

    void delete(String[] id);

    IPage<SystemGatewayRouter> findTByPage(SystemGatewayRouterPage user);

    List<SystemGatewayRouter> selectByParams(SystemGatewayRouter systemUser);

    void enable(String[] id, int state);
}
