package com.bucket.cloud.system.service;


public interface RoleResourcesService {

    void deleteByRoleId(String[] id);

    void deleteByResourcesId(String[] id);

    void add(String roleId, String[] resourcesIds);
}
