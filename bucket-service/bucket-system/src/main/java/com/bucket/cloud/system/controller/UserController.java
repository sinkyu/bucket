package com.bucket.cloud.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bucket.cloud.common.core.annotation.CustomLog;
import com.bucket.cloud.common.core.controller.BaseController;
import com.bucket.cloud.common.core.model.LogType;
import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.common.core.security.OpenHelper;
import com.bucket.cloud.system.api.model.SystemUser;
import com.bucket.cloud.system.api.model.SystemUserAddModel;
import com.bucket.cloud.system.api.model.UserPage;
import com.bucket.cloud.system.api.service.ISystemUserServiceClient;
import com.bucket.cloud.system.api.vo.SystemUserVO;
import com.bucket.cloud.system.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Api(value = "用户管理")
@RestController
@RequestMapping("/system/user")
public class UserController extends BaseController implements ISystemUserServiceClient {

    @Autowired
    private UserService userService;

    @ApiOperation(value = "列表", httpMethod = "POST")
    @RequestMapping(value = "/list")
    @CustomLog(name = "查询用户", description = "查询用户列表", type = LogType.QUERY)
    public Result list(@RequestBody UserPage user) {
        return Result.ok().data(userService.findTByPage(user));
    }

    @ApiOperation(value = "查所有", httpMethod = "POST")
    @RequestMapping(value = "/all")
    public Result all() {
        return Result.ok().data(userService.list(new QueryWrapper<SystemUser>()));
    }

    @ApiOperation(value = "是否重复", httpMethod = "POST")
    @RequestMapping(value = "/isexist")
    public Result checkExist(@RequestBody SystemUser systemUser) {
        List<SystemUser> tByT = userService.selectByParams(systemUser);
        boolean r = tByT.size() != 0;
        return Result.ok().data(r);
    }

    @ApiOperation(value = "用户菜单", httpMethod = "POST")
    @RequestMapping(value = "/getInfo")
    public Result getInfoWithResources(@RequestBody SystemUser systemUser) {
        systemUser.setUsername(OpenHelper.getUser().getUsername());
        SystemUser returnUser = userService.getThisUserInfo(systemUser);
        return Result.ok().data(returnUser);
    }

    @ApiOperation(value = "增加", httpMethod = "POST")
    @RequestMapping(value = "/add")
    @CustomLog(name = "新增用户", description = "新增用户", type = LogType.ADD)
    public Result add(@RequestBody SystemUserAddModel user) {
        userService.insertUser(user);
        return Result.ok();

    }

    @ApiOperation(value = "更新", httpMethod = "POST")
    @RequestMapping(value = "/update")
    @CustomLog(name = "修改用户", description = "修改用户", type = LogType.EDIT)
    public Result update(@RequestBody SystemUserAddModel muser) {
        Assert.notNull(muser.getId(), "必填参数不能为空");
        userService.updateUser(muser);
        return Result.ok();

    }

    @ApiOperation(value = "删除", httpMethod = "POST")
    @RequestMapping(value = "/delete")
    @CustomLog(name = "删除用户", description = "删除用户", type = LogType.DELETE)
    public Result delete(@RequestBody SystemUser user) {
        userService.deleteUser(user.getId().split(","));
        return Result.ok();


    }

    @ApiOperation(value = "启用", httpMethod = "POST")
    @RequestMapping(value = "/enable")
    @CustomLog(name = "启用用户", description = "启用用户", type = LogType.EDIT)
    public Result enable(@RequestBody SystemUser user) {
        userService.enable(user.getId().split(","), 1);
        return Result.ok();

    }

    @ApiOperation(value = "禁用", httpMethod = "POST")
    @RequestMapping(value = "/disable")
    @CustomLog(name = "禁用用户", description = "禁用用户", type = LogType.EDIT)
    public Result disable(@RequestBody SystemUser user) {
        userService.enable(user.getId().split(","), 0);
        return Result.ok();

    }

    @ApiOperation(value = "更改密码", httpMethod = "POST")
    @RequestMapping(value = "/updatePassword")
    @CustomLog(name = "修改密码", description = "用户修改自己的密码", type = LogType.EDIT)
    public Result updatePassword(@RequestBody SystemUser user) {
        userService.updatePassword(user);
        return Result.ok();
    }

    @ApiOperation(value = "登录", notes = "仅限系统内部调用")
    @Override
    @PostMapping(value = "/login")
    public Result<SystemUserVO> login(@RequestBody SystemUser u) {
        return Result.ok().data(userService.getThisUserInfo(u));
    }

    @ApiOperation(value = "获取账号登录信息", notes = "仅限系统内部调用")
    @GetMapping(value = "/getUser")
    public Result<SystemUserVO> getUser() {
        return Result.ok().data(OpenHelper.getAuthUser());
    }
}
