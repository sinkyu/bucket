package com.bucket.cloud.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.common.core.model.JsonTreeData;
import com.bucket.cloud.system.api.model.ResourcesPage;
import com.bucket.cloud.system.api.model.SystemMenu;
import com.bucket.cloud.system.api.model.SystemResources;
import com.bucket.cloud.system.api.model.SystemUser;

import java.util.List;
import java.util.Map;

/**
 * 菜单管理
 *
 * @author cyq
 */
public interface ResourcesService {

    void deleteResources(String[] id);

    void insertResources(SystemResources systemResources);

    List<JsonTreeData> findResources(SystemResources resources);

    IPage<SystemResources> findResourcesPage(ResourcesPage resourcesPage);

    void enableResources(String[] id, int state);

    void updateResources(SystemResources systemResources);

    List<JsonTreeData> queryUserResources(SystemUser systemUser);

    /**
     * @auther: cyq
     * @Description: 获取菜单树结构
     * @Date: 2019/2/22 15:38
     */
    List<SystemMenu> getAllMenuList(SystemResources systemResources);

    SystemResources queryInfo(SystemResources param);

    List<SystemResources> queryBtnList(Map<String, Object> param);
}
