package com.bucket.cloud.system.controller;


import com.bucket.cloud.common.core.annotation.CustomLog;
import com.bucket.cloud.common.core.controller.BaseController;
import com.bucket.cloud.common.core.model.LogType;
import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.system.api.model.ResourcesPage;
import com.bucket.cloud.system.api.model.SystemResources;
import com.bucket.cloud.system.service.ResourcesService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

@Api(value = "权限管理")
@RestController
@RequestMapping("/system/resources")
public class ResourcesController extends BaseController {

    @Autowired
    private ResourcesService resourcesService;

    @ApiOperation(value = "增加", httpMethod = "POST")
    @RequestMapping(value = "/add")
    @CustomLog(name = "新增权限", description = "新增权限", type = LogType.ADD)
    public Result add(@RequestBody SystemResources systemResources) {
        resourcesService.insertResources(systemResources);
        return Result.ok();
    }

    @ApiOperation(value = "删除", httpMethod = "POST")
    @RequestMapping(value = "/delete")
    @CustomLog(name = "删除权限", description = "删除权限", type = LogType.DELETE)
    public Result delete(@RequestBody SystemResources systemResources) {
        resourcesService.deleteResources(systemResources.getId().split(","));
        return Result.ok();
    }

    @ApiOperation(value = "更新", httpMethod = "POST")
    @RequestMapping(value = "/update")
    @CustomLog(name = "修改权限", description = "修改权限", type = LogType.EDIT)
    public Result update(@RequestBody SystemResources systemResources) {
        resourcesService.updateResources(systemResources);
        return Result.ok();

    }

    @ApiOperation(value = "启用", httpMethod = "POST")
    @RequestMapping(value = "/enable")
    @CustomLog(name = "启用权限", description = "启用权限", type = LogType.EDIT)
    public Result enable(@RequestBody SystemResources resources) {
        resourcesService.enableResources(resources.getId().split(","), 1);
        return Result.ok();
    }

    @ApiOperation(value = "禁用", httpMethod = "POST")
    @RequestMapping(value = "/disable")
    @CustomLog(name = "禁用权限", description = "禁用权限", type = LogType.EDIT)
    public Result disable(@RequestBody SystemResources resources) {
        resourcesService.enableResources(resources.getId().split(","), 0);
        return Result.ok();
    }

    @ApiOperation(value = "列表", httpMethod = "POST")
    @RequestMapping(value = "/list")
    @CustomLog(name = "查询权限", description = "查询权限列表", type = LogType.QUERY)
    public Result list(@RequestBody ResourcesPage resources) {
        return Result.ok().data(resourcesService.findResourcesPage(resources));
    }

    @ApiOperation(value = "全部", httpMethod = "POST")
    @RequestMapping(value = "/all")
    public Result all(@RequestBody SystemResources resources) {
        return Result.ok().data(resourcesService.findResources(resources));
    }


    @ApiOperation(value = "菜单列表", httpMethod = "POST")
    @RequestMapping(value = "/menuList")
    public Result menuList(@RequestBody SystemResources resources) {
        return Result.ok().data(resourcesService.getAllMenuList(resources));
    }

    @ApiOperation(value = "查询资源信息", httpMethod = "POST")
    @RequestMapping(value = "/info")
    public Result queryInfo(@RequestBody SystemResources resources) {
        return Result.ok().data(resourcesService.queryInfo(resources));
    }

    @ApiOperation(value = "查询菜单按钮列表", httpMethod = "POST")
    @RequestMapping(value = "/btnList")
    public Result btnList(@RequestBody Map<String, Object> resources) {
        return Result.ok().data(resourcesService.queryBtnList(resources));
    }
}
