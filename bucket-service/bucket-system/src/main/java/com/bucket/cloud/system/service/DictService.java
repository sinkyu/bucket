package com.bucket.cloud.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bucket.cloud.common.core.model.JsonTreeData;
import com.bucket.cloud.system.api.model.DictPage;
import com.bucket.cloud.system.api.model.SystemDict;

import java.util.List;

/**
 * 字典管理
 *
 * @author cyq
 */
public interface DictService extends IService<SystemDict> {

    IPage<SystemDict> findTByPage(DictPage dict);

    List<JsonTreeData> findAll(SystemDict dict);

    void insertDict(SystemDict systemDict);

    void updateByPrimaryKeyDict(SystemDict systemDict);

    void deleteByPrimaryKeyDict(String[] id);

    void enable(String[] id, int state);

    /**
     * @auther: cyq
     * @Description: 根据父字典code获取所有的子项
     * @Date: 2019/3/1 14:06
     */
    List<SystemDict> getDictByParentCode(SystemDict dict);
}
