package com.bucket.cloud.system.controller;

import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.system.api.model.SystemGatewayIplimit;
import com.bucket.cloud.system.api.model.SystemGatewayIplimitPage;
import com.bucket.cloud.system.api.vo.SystemGatewayIplimitVO;
import com.bucket.cloud.system.service.GatewayIplimitService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(value = "白名单管理")
@RestController
@RequestMapping("/gateway/iplimit")
public class GatewayIplimitController {

    @Autowired
    GatewayIplimitService gatewayIplimitService;

    @ApiOperation(value = "分页查询访问控制", notes = "根据请求的参数分页查询访问控制列表")
    @RequestMapping(value = "/page")
    public Object iplimitPage(@RequestBody SystemGatewayIplimitPage params) {
        return Result.ok().data(gatewayIplimitService.findTByPage(params));
    }

    @ApiOperation(value = "查询访问控制列表", notes = "根据参数查询所有访问控制列表")
    @RequestMapping(value = "/list")
    public Object iplimitList(@RequestBody SystemGatewayIplimit params) {
        return Result.ok().data(gatewayIplimitService.selectByParams(params));
    }

    @ApiOperation(value = "查询访问控制信息", notes = "根据参数查询所有访问控制信息")
    @RequestMapping(value = "/info")
    public Object iplimitInfo(@RequestBody SystemGatewayIplimit params) {
        return Result.ok().data(gatewayIplimitService.queryInfo(params));
    }

    @ApiOperation(value = "新增访问控制", notes = "添加一个新的访问控制信息")
    @RequestMapping(value = "/add")
    public Object iplimitAdd(@RequestBody SystemGatewayIplimitVO params) {
        gatewayIplimitService.insert(params);
        return Result.ok();
    }

    @ApiOperation(value = "修改访问控制", notes = "根据参数修改访问控制信息")
    @RequestMapping(value = "/update")
    public Object iplimitUpdate(@RequestBody SystemGatewayIplimitVO params) {
        gatewayIplimitService.update(params);
        return Result.ok();
    }

    @ApiOperation(value = "删除访问控制", notes = "根据提供的id，批量删除访问控制")
    @RequestMapping(value = "/del")
    public Object iplimitDel(@RequestBody SystemGatewayIplimit params) {
        gatewayIplimitService.delete(params.getId().split(","));
        return Result.ok();
    }

    @ApiOperation(value = "启用访问控制", notes = "根据提供的id，批量启用访问控制")
    @RequestMapping(value = "/enable")
    public Object iplimitEnable(@RequestBody SystemGatewayIplimit params) {
        gatewayIplimitService.enable(params.getId().split(","),1);
        return Result.ok();
    }

    @ApiOperation(value = "禁用访问控制", notes = "根据提供的id，批量禁用访问控制")
    @RequestMapping(value = "/disable")
    public Object iplimitDisable(@RequestBody SystemGatewayIplimit params) {
        gatewayIplimitService.enable(params.getId().split(","),0);
        return Result.ok();
    }
}
