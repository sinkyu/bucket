package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.bucket.cloud.system.api.model.SystemIplimitInterface;

/**
 * 接口限制
 */
public interface IplimitIntMapper extends BaseMapper<SystemIplimitInterface> {
}
