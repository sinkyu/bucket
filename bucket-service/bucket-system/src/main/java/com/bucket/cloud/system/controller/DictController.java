package com.bucket.cloud.system.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.bucket.cloud.common.core.annotation.CustomLog;
import com.bucket.cloud.common.core.controller.BaseController;
import com.bucket.cloud.common.core.model.LogType;
import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.system.api.model.DictPage;
import com.bucket.cloud.system.api.model.SystemDict;
import com.bucket.cloud.system.service.DictService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

@Api(value = "字典管理")
@RestController
@RequestMapping("/system/dict")
public class DictController extends BaseController {

    @Autowired
    private DictService dictService;

    @ApiOperation(value = "字典列表", httpMethod = "POST")
    @RequestMapping(value = "/list")
    @CustomLog(name = "查询字典", description = "查询字典列表", type = LogType.QUERY)
    public Result list(@RequestBody DictPage dict) {
        return Result.ok().data(dictService.findTByPage(dict));
    }

    @ApiOperation(value = "查全部字典", httpMethod = "POST")
    @RequestMapping(value = "/all")
    public Result all(@RequestBody SystemDict dict) {
        return Result.ok().data(dictService.findAll(dict));
    }

    @ApiOperation(value = "新增字典", httpMethod = "POST")
    @RequestMapping(value = "/add")
    @CustomLog(name = "新增字典", description = "新增了一条字典", type = LogType.ADD)
    @CacheEvict(value = "dicts", key = "#systemDict.type")
    public Result add(@RequestBody SystemDict systemDict) {
        dictService.insertDict(systemDict);
        return Result.ok();
    }

    @ApiOperation(value = "修改字典", httpMethod = "POST")
    @RequestMapping(value = "/update")
    @CustomLog(name = "修改字典", description = "修改了一条字典", type = LogType.EDIT)
    @CacheEvict(value = "dicts", key = "#systemDict.type")
    public Result update(@RequestBody SystemDict systemDict) {
        Assert.notNull(systemDict.getId(), "缺少参数！");
        dictService.updateByPrimaryKeyDict(systemDict);
        return Result.ok();
    }

    @ApiOperation(value = "删除字典", httpMethod = "POST")
    @RequestMapping(value = "/delete")
    @CustomLog(name = "删除字典", description = "删除了N条字典", type = LogType.DELETE)
    @CacheEvict(value = "dicts")
    public Result delete(@RequestBody SystemDict dict) {
        dictService.deleteByPrimaryKeyDict(dict.getId().split(","));
        return Result.ok();

    }

    @ApiOperation(value = "启用字典", httpMethod = "POST")
    @RequestMapping(value = "/enable")
    @CustomLog(name = "启用字典", description = "启用了N条字典", type = LogType.EDIT)
    @CacheEvict(value = "dicts")
    public Result enable(@RequestBody SystemDict dict) {
        dictService.enable(dict.getId().split(","), 1);
        return Result.ok();
    }

    @ApiOperation(value = "禁用字典", httpMethod = "POST")
    @RequestMapping(value = "/disable")
    @CustomLog(name = "禁用字典", description = "禁用了N条字典", type = LogType.EDIT)
    @CacheEvict(value = "dicts")
    public Result disable(@RequestBody SystemDict dict) {
        dictService.enable(dict.getId().split(","), 0);
        return Result.ok();
    }

    @ApiOperation(value = "根据上级CODE获取列表", httpMethod = "POST")
    @RequestMapping(value = "/get")
    public Result get(@RequestBody SystemDict dict) {
        return Result.ok().data(dictService.getDictByParentCode(dict));
    }

    @ApiOperation(value = "根据字段类型查询", httpMethod = "GET")
    @GetMapping(value = "/getByType/{type}")
    @Cacheable(value = "dicts", key = "#type")
    public Result getByType(@PathVariable String type) {
        Assert.notNull(type, "字典查询类型不能为空！");
        return Result.ok().data(dictService.list(new QueryWrapper<SystemDict>().eq("TYPE", type).eq("STATE", 1)));
    }
}
