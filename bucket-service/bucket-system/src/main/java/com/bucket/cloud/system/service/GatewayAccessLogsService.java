package com.bucket.cloud.system.service;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.common.core.model.PageParams;
import com.bucket.cloud.system.api.model.GatewayAccessLogs;

/**
 * 网关访问日志
 *
 * @author cyq
 */
public interface GatewayAccessLogsService {
    /**
     * 分页查询
     *
     * @param pageParams
     * @return
     */
    IPage<GatewayAccessLogs> findListPage(PageParams pageParams);
}
