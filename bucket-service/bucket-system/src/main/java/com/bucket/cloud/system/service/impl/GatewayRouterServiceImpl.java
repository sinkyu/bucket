package com.bucket.cloud.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bucket.cloud.system.api.model.SystemGatewayRouter;
import com.bucket.cloud.system.api.model.SystemGatewayRouterPage;
import com.bucket.cloud.system.dao.GatewayRouterMapper;
import com.bucket.cloud.system.service.GatewayRouterService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;

@Service
public class GatewayRouterServiceImpl implements GatewayRouterService {

    @Resource
    GatewayRouterMapper gatewayRouterMapper;

    @Resource
    private RedisTemplate redisTemplate;

    @Override
    public SystemGatewayRouter selectByPrimaryKey(String id) {
        return gatewayRouterMapper.selectById(id);
    }

    @Override
    public void insert(SystemGatewayRouter params) {
        params.setId(UUID.randomUUID().toString().replaceAll("-", ""));
        gatewayRouterMapper.insert(params);
    }

    @Override
    public void update(SystemGatewayRouter params) {
        gatewayRouterMapper.updateById(params);
    }

    @Override
    public void delete(String[] id) {
        gatewayRouterMapper.deleteBatchIds(Arrays.asList(id));
        for (String i : id) {
            redisTemplate.opsForHash().delete("geteway_routes", i);
        }
    }

    @Override
    public IPage<SystemGatewayRouter> findTByPage(SystemGatewayRouterPage params) {
        IPage pageObj = new Page(params.getPageNumber(), params.getPageSize());
        return gatewayRouterMapper.queryByParams(pageObj, params);
    }

    @Override
    public List<SystemGatewayRouter> selectByParams(SystemGatewayRouter params) {
        return gatewayRouterMapper.queryByParams(params);
    }

    @Override
    public void enable(String[] id, int state) {
        for (String i : id) {
            SystemGatewayRouter rs = new SystemGatewayRouter();
            rs.setId(i);
            rs.setState(state);
            gatewayRouterMapper.updateById(rs);
        }
    }
}
