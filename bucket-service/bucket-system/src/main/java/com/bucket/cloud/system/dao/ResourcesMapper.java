package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.system.api.model.ResourcesPage;
import com.bucket.cloud.system.api.model.SystemResources;
import com.bucket.cloud.system.api.model.SystemUser;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 菜单管理
 */
public interface ResourcesMapper extends BaseMapper<SystemResources> {

    IPage<SystemResources> queryByParams(IPage<SystemResources> page, @Param("ew") ResourcesPage resources);

    List<SystemResources> queryUserResources(SystemUser user);
}