package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.system.api.model.SystemGatewayRouter;
import com.bucket.cloud.system.api.model.SystemGatewayRouterPage;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 路由管理
 */
public interface GatewayRouterMapper extends BaseMapper<SystemGatewayRouter> {

    IPage<SystemGatewayRouter> queryByParams(IPage<SystemGatewayRouter> page, @Param("ew") SystemGatewayRouterPage entity);

    List<SystemGatewayRouter> queryByParams(@Param("ew") SystemGatewayRouter entity);
}