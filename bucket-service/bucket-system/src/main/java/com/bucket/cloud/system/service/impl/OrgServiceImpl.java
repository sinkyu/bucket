package com.bucket.cloud.system.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.bucket.cloud.common.core.model.Attributes;
import com.bucket.cloud.common.core.model.JsonTreeData;
import com.bucket.cloud.common.core.util.TreeNodeUtil;
import com.bucket.cloud.system.api.model.OrgPage;
import com.bucket.cloud.system.api.model.SystemOrg;
import com.bucket.cloud.system.dao.OrgMapper;
import com.bucket.cloud.system.service.OrgService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * code is far away from bug with the animal protecting
 * ┏┛ ┻━━━━━┛ ┻┓
 * ┃　　　　　　 ┃
 * ┃　　　━　　　┃
 * ┃　┳┛　  ┗┳　┃
 * ┃　　　　　　 ┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　 ┃
 * ┗━┓　　　┏━━━┛
 * ┃　　　┃   神兽保佑
 * ┃　　　┃   代码无BUG！
 * ┃　　　┗━━━━━━━━━┓
 * ┃　　　　　　　    ┣┓
 * ┃　　　　         ┏┛
 * ┗━┓ ┓ ┏━━━┳ ┓ ┏━┛
 * ┃ ┫ ┫   ┃ ┫ ┫
 * ┗━┻━┛   ┗━┻━┛
 *
 * @Description :
 * ---------------------------------
 * @Author : cyq
 * @Date : 2018/11/17 16:15
 */
@Service
@Transactional
public class OrgServiceImpl implements OrgService {

    @Resource
    OrgMapper orgMapper;

    @Override
    public void add(SystemOrg org) {
        orgMapper.insert(org);
    }

    @Override
    public void del(String[] id) {
        orgMapper.deleteBatchIds(Arrays.asList(id));
    }

    @Override
    public void edit(SystemOrg org) {
        orgMapper.updateById(org);
    }

    @Override
    public void enable(String[] id, int state) {
        SystemOrg org = new SystemOrg();
        org.setState(state);
        orgMapper.update(org, new QueryWrapper<SystemOrg>().in("id", id));
    }

    @Override
    public IPage<SystemOrg> queryPage(OrgPage orgPage) {
        IPage pageObj = new Page(orgPage.getPageNumber(), orgPage.getPageSize());
        return orgMapper.queryByParams(pageObj, orgPage);
    }

    @Override
    public List<JsonTreeData> queryAll(SystemOrg org) {
        List<SystemOrg> systemResourcesList = orgMapper.queryTreeByParams(org);
        return resourcesToJsonTreeData(systemResourcesList, org.getCode() != null ? org.getCode() : null);
    }

    @Override
    public List<SystemOrg> get(SystemOrg org) {
        return orgMapper.selectList(new QueryWrapper<SystemOrg>()
                .eq("pcode", org.getPcode())
                .or()
                .eq("TYPE", org.getType())
                .orderByAsc("SORT"));
    }

    private List<JsonTreeData> resourcesToJsonTreeData(List<SystemOrg> systemResourcesList, String parentCode) {
        List<JsonTreeData> treeDataList = new ArrayList<JsonTreeData>();
        /*为了整理成公用的方法，所以将查询结果进行二次转换。
         * 其中specid为主键ID，varchar类型UUID生成
         * parentid为父ID
         * specname为节点名称
         * */
        for (SystemOrg htSpecifications : systemResourcesList) {
            JsonTreeData treeData = new JsonTreeData(htSpecifications.getCode()
                    , htSpecifications.getPcode(), htSpecifications.getShortName()
                    , null, htSpecifications.getState()
                    , new Attributes(), null);
            treeDataList.add(treeData);
        }
        //最后得到结果集,经过FirstJSON转换后就可得所需的json格式
        List<JsonTreeData> newTreeDataList = null;
        if (parentCode == null)
            newTreeDataList = TreeNodeUtil.getfatherNode(treeDataList);
        else
            newTreeDataList = TreeNodeUtil.getfatherNode(treeDataList, parentCode);
        return newTreeDataList;
    }
}
