package com.bucket.cloud.system.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bucket.cloud.common.core.model.Attributes;
import com.bucket.cloud.common.core.model.JsonTreeData;
import com.bucket.cloud.common.core.util.TreeNodeUtil;
import com.bucket.cloud.system.api.model.DictPage;
import com.bucket.cloud.system.api.model.SystemDict;
import com.bucket.cloud.system.dao.DictMapper;
import com.bucket.cloud.system.service.DictService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
@Transactional
public class DictServiceImpl extends ServiceImpl<DictMapper, SystemDict> implements DictService {

    @Resource
    private DictMapper dictMapper;

    @Override
    public IPage<SystemDict> findTByPage(DictPage dict) {
        IPage<SystemDict> page = new Page<>(dict.getPageNumber(), dict.getPageSize());
        return dictMapper.queryByParams(page, dict);
    }

    @Override
    public List<JsonTreeData> findAll(SystemDict dict) {
        List<SystemDict> systemResourcesList = dictMapper.queryTreeByParams(dict);
        return resourcesToJsonTreeData(systemResourcesList, dict.getCode() != null ? dict.getCode() : null);
    }

    @Override
    public void insertDict(SystemDict systemDict) {
        dictMapper.insert(systemDict);
    }

    @Override
    public void updateByPrimaryKeyDict(SystemDict systemDict) {
        dictMapper.updateById(systemDict);
    }

    @Override
    public void deleteByPrimaryKeyDict(String[] id) {
        dictMapper.deleteBatchIds(Arrays.asList(id));
    }

    @Override
    public void enable(String[] id, int state) {
        for (String i : id) {
            SystemDict rs = new SystemDict();
            rs.setId(i);
            rs.setState(state);
            dictMapper.updateById(rs);
        }
    }

    @Override
    public List<SystemDict> getDictByParentCode(SystemDict dict) {
        return dictMapper.queryTreeByParams(dict);
    }

    private List<JsonTreeData> resourcesToJsonTreeData(List<SystemDict> systemResourcesList, String parentCode) {
        List<JsonTreeData> treeDataList = new ArrayList<JsonTreeData>();
        /*为了整理成公用的方法，所以将查询结果进行二次转换。
         * 其中specid为主键ID，varchar类型UUID生成
         * parentid为父ID
         * specname为节点名称
         * */
        for (SystemDict htSpecifications : systemResourcesList) {
            JsonTreeData treeData = new JsonTreeData(htSpecifications.getCode()
                    , htSpecifications.getType(), htSpecifications.getName()
                    , null, htSpecifications.getState()
                    , new Attributes(), null);
            treeDataList.add(treeData);
        }
        //最后得到结果集,经过FirstJSON转换后就可得所需的json格式
        List<JsonTreeData> newTreeDataList = null;
        if (parentCode == null)
            newTreeDataList = TreeNodeUtil.getfatherNode(treeDataList);
        else
            newTreeDataList = TreeNodeUtil.getfatherNode(treeDataList, parentCode);
        return newTreeDataList;
    }
}
