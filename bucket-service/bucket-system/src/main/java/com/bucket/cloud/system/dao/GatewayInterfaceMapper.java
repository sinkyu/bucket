package com.bucket.cloud.system.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.system.api.model.SystemInterface;
import com.bucket.cloud.system.api.model.SystemInterfacePage;
import com.bucket.cloud.system.api.model.SystemUser;
import com.bucket.cloud.system.api.vo.SystemInterfaceVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * api管理
 */
public interface GatewayInterfaceMapper extends BaseMapper<SystemInterface> {

    List<SystemInterfaceVO> queryApiList(IPage<SystemInterface> page, @Param("ew") SystemInterfacePage params);

    List<SystemInterfaceVO> queryApiList(@Param("ew") SystemInterfaceVO params);

    List<SystemInterface> queryAllApi();

    List<SystemInterface> queryApiByClientId(@Param("clientId") String clientId);

    void mergeApi(SystemInterface params);

    List<SystemInterface> queryUserApi(SystemUser params);

    List<SystemInterfaceVO> queryApiListForParams(SystemInterfaceVO params);
}
