package com.bucket.cloud.system.controller;

import com.bucket.cloud.common.core.controller.BaseController;
import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.system.api.model.SystemAuthApi;
import com.bucket.cloud.system.api.model.SystemInterface;
import com.bucket.cloud.system.api.model.SystemInterfacePage;
import com.bucket.cloud.system.api.model.SystemUser;
import com.bucket.cloud.system.api.service.IGatewayInterfaceServiceClient;
import com.bucket.cloud.system.api.vo.SystemGatewayIplimitVO;
import com.bucket.cloud.system.api.vo.SystemInterfaceVO;
import com.bucket.cloud.system.service.GatewayInterfaceService;
import com.bucket.cloud.system.service.GatewayIplimitService;
import com.bucket.cloud.system.service.GatewayRouterService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import java.util.List;

@Api(value = "Api管理")
@RestController
@RequestMapping("/gateway/api/interface")
public class GatewayInterfaceController extends BaseController implements IGatewayInterfaceServiceClient {

    @Autowired
    GatewayInterfaceService gatewayInterfaceService;
    @Autowired
    GatewayIplimitService gatewayIplimitService;
    @Autowired
    GatewayRouterService gatewayRouterService;

    @ApiOperation(value = "查询所有接口", notes = "根据请求的参数查询所有接口的列表，多用于授权页面读取列表")
    @RequestMapping(value = "/allList")
    public Object queryAllList(@RequestBody SystemInterfaceVO params) {
        return gatewayInterfaceService.queryApiList(params);
    }

    @ApiOperation(value = "绑定接口", notes = "根据请求的参数给指定对象绑定指定接口")
    @RequestMapping(value = "/bind")
    public Object bind(@RequestBody SystemAuthApi params) {
        return gatewayInterfaceService.bindApi(params);
    }

    @ApiOperation(value = "分页查询接口", notes = "根据请求的参数分页查询接口API列表")
    @RequestMapping(value = "/page")
    public Object page(@RequestBody SystemInterfacePage params) {
        return Result.ok().data(gatewayInterfaceService.findByPage(params));
    }

    @ApiOperation(value = "新增接口", notes = "添加一个新的接口API信息")
    @RequestMapping(value = "/add")
    public Object add(@RequestBody SystemInterface params) {
        return gatewayInterfaceService.addApi(params);
    }

    @ApiOperation(value = "修改接口", notes = "根据参数修改接口API信息")
    @RequestMapping(value = "/update")
    public Object update(@RequestBody SystemInterface params) {
        return gatewayInterfaceService.updateApi(params);
    }

    @ApiOperation(value = "删除接口", notes = "根据提供的id，批量删除接口API")
    @RequestMapping(value = "/del")
    public Object del(@RequestBody SystemInterface params) {
        return gatewayInterfaceService.delApi(params.getId().split(","));
    }

    @ApiOperation(value = "启用接口", notes = "根据提供的id，批量启用接口")
    @RequestMapping(value = "/enable")
    public Object enable(@RequestBody SystemInterface params) {
        return gatewayInterfaceService.enableApi(params.getId().split(","), 1);
    }

    @ApiOperation(value = "禁用接口", notes = "根据提供的id，批量禁用接口API")
    @RequestMapping(value = "/disable")
    public Object disable(@RequestBody SystemInterface params) {
        return gatewayInterfaceService.enableApi(params.getId().split(","), 0);
    }

    @ApiIgnore
    @Override
    @PostMapping(value = "/userApiList")
    public Result<List<SystemInterface>> queryUserApis(SystemUser params) {
        return Result.ok().data(gatewayInterfaceService.queryUserApi(params));
    }

    //    @ApiOperation(value = "查询接口列表（非公开）", notes = "根据参数查询系统登记在册的接口列表")
    @ApiIgnore
    @Override
    @PostMapping(value = "/apiList")
    public Result<List<SystemInterfaceVO>> queryApiAllList(SystemInterfaceVO params) {
        return gatewayInterfaceService.queryApiAllList(params);
    }

    /**
     * @auther: cyq
     * @Description: 查询所有接口列表
     * @Date: 2020/6/11 11:29
     */
    @ApiIgnore
    @Override
    @PostMapping(value = "/allApiList")
    public Result<List<SystemInterface>> queryAllApi() {
        return gatewayInterfaceService.queryAllApi();
    }

    /**
     * @auther: cyq
     * @Description: 查询IP受限接口列表（非公开）
     * @Date: 2020/6/16 17:10
     */
    @ApiIgnore
    @Override
    @PostMapping(value = "/limitApiList")
    public Result<List<SystemInterfaceVO>> queryLimitApiList(SystemGatewayIplimitVO params) {
        return gatewayIplimitService.queryLimitApiList(params);
    }

}
