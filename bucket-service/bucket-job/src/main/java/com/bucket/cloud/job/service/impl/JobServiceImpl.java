package com.bucket.cloud.job.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.bucket.cloud.job.api.model.QuartzEntity;
import com.bucket.cloud.job.api.model.QuartzEntityPage;
import com.bucket.cloud.job.dao.JobMapper;
import com.bucket.cloud.job.service.IJobService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @Auther: cyq
 * @Date: 2018/10/24 11:07
 * @Description:
 */
@Service
public class JobServiceImpl extends ServiceImpl<JobMapper, QuartzEntity> implements IJobService {

    @Resource
    JobMapper jobMapper;

    @Override
    public IPage<QuartzEntity> listQuartzEntity(QuartzEntityPage quartz) {
        IPage<QuartzEntity> page = new Page<>(quartz.getPageNumber(), quartz.getPageSize());
        return jobMapper.queryByParams(page,quartz);
    }

}
