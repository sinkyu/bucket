package com.bucket.cloud.job;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * 任务调度服务
 * 提供系统任务调度功能
 *
 * @author cyq
 */
@EnableCaching
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@MapperScan(basePackages = "com.bucket.cloud.job.dao")
public class BucketJobApplication {

    public static void main(String[] args) {
        SpringApplication.run(BucketJobApplication.class, args);
        System.err.println("==============任务调度服务启动成功==============");
    }

}