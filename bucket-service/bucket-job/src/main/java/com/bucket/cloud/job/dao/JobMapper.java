package com.bucket.cloud.job.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.bucket.cloud.job.api.model.QuartzEntity;
import com.bucket.cloud.job.api.model.QuartzEntityPage;
import org.apache.ibatis.annotations.Param;

/**
 * @Auther: cyq
 * @Date: 2018/10/24 10:32
 * @Description:
 */
public interface JobMapper extends BaseMapper<QuartzEntity> {

    /**
     * @Author cyq
     * @Description 分页查询定时任务
     * @Date 2020/7/22 12:31 上午
     **/
    IPage<QuartzEntity> queryByParams(IPage<QuartzEntity> page, @Param("ew") QuartzEntityPage quartz);

}
