package com.bucket.cloud.job.service;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.bucket.cloud.job.api.model.QuartzEntity;
import com.bucket.cloud.job.api.model.QuartzEntityPage;

public interface IJobService extends IService<QuartzEntity> {

    IPage<QuartzEntity> listQuartzEntity(QuartzEntityPage quartz);
    
}
