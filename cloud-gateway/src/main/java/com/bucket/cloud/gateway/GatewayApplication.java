package com.bucket.cloud.gateway;

import com.bucket.cloud.gateway.event.GatewayRemoteRefreshRouteEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.bus.BusProperties;
import org.springframework.cloud.bus.jackson.RemoteApplicationEventScan;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.ApplicationEventPublisher;

/**
 * 网关服务
 * 接口调用统一入口、数字验签、身份认证、接口鉴权、接口限流、黑白名单限制
 * 开发环境下提供在线调试文档.
 * @author cyq
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
@RemoteApplicationEventScan(basePackages = "com.bucket.cloud")
public class GatewayApplication implements CommandLineRunner {

    @Autowired
    public ApplicationEventPublisher publisher;
    @Autowired
    private BusProperties bus;

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

    /**
     * @auther: cyq
     * @Description: 加载路由信息
     * @Date: 2019/9/7 14:06
     */
    @Override
    public void run(String... strings) throws Exception {
//        jdbcRouteDefinitionLocator.refresh();
//        resourceLocator.refresh();
        // 消息总线刷新所有网关实例
        publisher.publishEvent(new GatewayRemoteRefreshRouteEvent(this, bus.getId(), null));
    }
}
