package com.bucket.cloud.gateway.exception;

import com.bucket.cloud.common.core.exception.BucketSignatureException;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

public interface ServerSignatureDeniedHandler {

    Mono<Void> handle(ServerWebExchange var1, BucketSignatureException var2);
}