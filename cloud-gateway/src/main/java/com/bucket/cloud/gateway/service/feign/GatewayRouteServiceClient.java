package com.bucket.cloud.gateway.service.feign;

import com.bucket.cloud.system.api.constant.SystemConstants;
import com.bucket.cloud.system.api.service.IGatewayRouteServiceClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

@Component
@FeignClient(value = SystemConstants.BASE_SERVER)
public interface GatewayRouteServiceClient extends IGatewayRouteServiceClient {

}
