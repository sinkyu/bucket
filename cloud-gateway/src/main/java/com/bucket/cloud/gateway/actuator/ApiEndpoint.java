package com.bucket.cloud.gateway.actuator;

import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.gateway.event.GatewayRemoteRefreshRouteEvent;
import org.springframework.boot.actuate.endpoint.web.annotation.RestControllerEndpoint;
import org.springframework.cloud.bus.endpoint.AbstractBusEndpoint;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * 自定义网关监控端点
 * @author cyq
 */
@RestControllerEndpoint(
        id = "open"
)
public class ApiEndpoint extends AbstractBusEndpoint {

    public ApiEndpoint(ApplicationEventPublisher context, String id) {
        super(context, id);
    }

    /**
     * 支持灰度发布
     * /actuator/open/refresh?destination = customers：**
     *
     * @param destination
     */
    @PostMapping("/refresh")
    public Result busRefreshWithDestination(@RequestParam(required = false)  String destination) {
        this.publish(new GatewayRemoteRefreshRouteEvent(this, this.getInstanceId(), destination));
        return Result.ok();
    }
}
