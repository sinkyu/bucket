package com.bucket.cloud.system.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.bucket.cloud.common.core.model.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "DictPage", description = "字典分页")
@JsonIgnoreProperties(value = {"handler"})
public class DictPage extends Page implements Serializable {

    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "类型")
    private String type;
    @ApiModelProperty(value = "编码")
    private String code;
    @ApiModelProperty(value = "状态（0禁用，1启用）")
    private Integer state;
    @ApiModelProperty(value = "序号")
    private Integer sort;
    @ApiModelProperty(value = "备注")
    private String remark;
}