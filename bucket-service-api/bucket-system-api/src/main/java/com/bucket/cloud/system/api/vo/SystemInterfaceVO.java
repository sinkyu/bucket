package com.bucket.cloud.system.api.vo;

import com.bucket.cloud.system.api.model.SystemGatewayIplimit;
import com.bucket.cloud.system.api.model.SystemInterface;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SystemInterfaceVO extends SystemInterface {

    private String serviceId;

    private SystemGatewayIplimit iplimit;

    private String limitType;

    private Set<String> ipAddressSet;

    private Integer authed;

    private String authType;

    private String authId;

    private Integer iplimitState;

    public void setIpAddressSet(String ipAddress) {
        if(StringUtils.isNotBlank(ipAddress)){
            ipAddressSet =  new HashSet<>(Arrays.asList(ipAddress.split(";")));
        }
    }
}
