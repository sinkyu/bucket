package com.bucket.cloud.system.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.bucket.cloud.common.core.model.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "ResourcesPage", description = "权限分页")
@JsonIgnoreProperties(value = {"handler"})
public class ResourcesPage extends Page implements Serializable {

    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "状态（0禁用，1启用）")
    private Integer state;
    @ApiModelProperty(value = "父id")
    private String pid;
    @ApiModelProperty(value = "名称")
    private String name;
    @ApiModelProperty(value = "链接")
    private String url;
    @ApiModelProperty(value = "代码")
    private String code;
    @ApiModelProperty(value = "类型（菜单，按钮）")
    private String type;
    @ApiModelProperty(value = "按钮样式")
    private String btnClass;
    @ApiModelProperty(value = "排序")
    private Integer sort;

}