package com.bucket.cloud.system.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SystemGatewayRouter implements Serializable {

    private String id;
    private String routId;
    private String routName;
    private String routPath;
    private String routType;
    private String routUrl;
    private Integer stripPrefix;
    private Integer routRetry;
    private String retrySeries;
    private String retryStatus;
    private String retryMethod;
    private String retryExcep;
    private Integer state;
}
