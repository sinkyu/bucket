package com.bucket.cloud.system.api.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SystemIplimitInterface implements Serializable {

    private String id;
    private String limitId;
    private String interfaceId;
}
