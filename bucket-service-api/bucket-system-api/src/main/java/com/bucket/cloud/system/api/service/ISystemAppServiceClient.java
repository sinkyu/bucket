package com.bucket.cloud.system.api.service;

import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.common.core.security.OpenClientDetails;
import com.bucket.cloud.system.api.model.SystemApp;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @author cyq
 */
public interface ISystemAppServiceClient {

    /**
     * 获取应用基础信息
     *
     * @param aid 应用Id
     * @return
     */
    @GetMapping("/system/app/{aid}/info")
    Result<SystemApp> getApp(@PathVariable("aid") String aid);

    /**
     * 获取应用开发配置信息
     * @param clientId
     * @return
     */
    @GetMapping("/system/app/client/{clientId}/info")
    Result<OpenClientDetails> getAppClientInfo(@PathVariable("clientId") String clientId);
}
