package com.bucket.cloud.system.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "SystemRoleAddModel", description = "角色权限")
@JsonIgnoreProperties(value = {"handler"})
public class SystemRoleAddModel extends SystemRole implements Serializable {

    @ApiModelProperty(value = "权限数组")
    private String[] resourcesId;

    public SystemRole getSystemRole() {
        SystemRole sr = new SystemRole();
        sr.setId(this.getId());
        sr.setCode(this.getCode());
        sr.setName(this.getName());
        sr.setState(this.getState());
        return sr;
    }
}