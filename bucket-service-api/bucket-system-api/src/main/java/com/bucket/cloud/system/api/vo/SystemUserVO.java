package com.bucket.cloud.system.api.vo;

import com.bucket.cloud.common.core.model.JsonTreeData;
import com.bucket.cloud.system.api.model.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SystemUserVO extends SystemUser {
    private List<SystemRole> roles;
    private List<SystemResources> resources;
    private List<SystemDict> policeTypes;
    private List<SystemDict> positions;
    private List<SystemUserResources> otherResources;
    private String currentOrgName;
    private String temporaryOrgName;
    private List<JsonTreeData> allResourcesTree;
    private List<SystemInterface> apis;

    private String mobile;
    private String deptCode;
}
