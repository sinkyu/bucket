package com.bucket.cloud.system.api.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * code is far away from bug with the animal protecting
 * ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃代码无BUG！
 * 　　┃　　　┗━━━┓
 * 　　┃　　　　　　　┣┓
 * 　　┃　　　　　　　┏┛
 * 　　┗┓┓┏━┳┓┏┛
 * 　　　┃┫┫　┃┫┫
 * 　　　┗┻┛　┗┻┛
 *
 * @Description :
 * ---------------------------------
 * @Author : cyq
 * @Date : Create in 2018/11/14 18:29
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "SystemOrg", description = "单位")
@JsonIgnoreProperties(value = {"handler"})
@TableName("SYSTEM_ORG")
public class SystemOrg implements Serializable {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "state")
    private Integer state;

    @ApiModelProperty(value = "code")
    private String code;

    @ApiModelProperty(value = "name")
    private String name;

    @ApiModelProperty(value = "pcode")
    private String pcode;

    @ApiModelProperty(value = "type")
    private String type;

    @ApiModelProperty(value = "shortName")
    @TableField(value = "SHORT_NAME")
    private String shortName;

    @ApiModelProperty(value = "sort")
    private Integer sort;

    @TableField(exist = false)
    private String deptName;
}
