package com.bucket.cloud.system.api.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(value = {"handler"})
@TableName("SYSTEM_USER_ROLE")
public class SystemUserRole implements Serializable {
    private String id;

    @TableField(value = "USER_ID")
    private String userId;

    @TableField(value = "ROLE_ID")
    private String roleId;
}