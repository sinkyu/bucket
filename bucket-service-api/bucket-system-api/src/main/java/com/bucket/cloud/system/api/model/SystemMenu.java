package com.bucket.cloud.system.api.model;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "SystemResources", description = "系统管理-菜单管理")
@JsonIgnoreProperties(value = {"handler"})
@TableName("SYSTEM_RESOURCES")
public class SystemMenu {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "父id")
    private String pid;

    @ApiModelProperty(value = "排序")
    private Integer sort;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "是否展开")
    private Boolean open;

    @ApiModelProperty(value = "子节点集合")
    private List<SystemMenu> lists;

    @ApiModelProperty(value = "链接地址")
    private String url;

    @ApiModelProperty(value = "代码")
    private String code;

    @ApiModelProperty(value = "类型（菜单，按钮）")
    private String type;

    @ApiModelProperty(value = "状态（0禁用，1启用）")
    private Integer state;


}
