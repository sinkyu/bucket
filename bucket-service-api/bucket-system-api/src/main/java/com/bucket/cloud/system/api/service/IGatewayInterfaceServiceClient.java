package com.bucket.cloud.system.api.service;

import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.system.api.model.SystemInterface;
import com.bucket.cloud.system.api.model.SystemUser;
import com.bucket.cloud.system.api.vo.SystemGatewayIplimitVO;
import com.bucket.cloud.system.api.vo.SystemInterfaceVO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

/**
 * @Auther: cyq
 * @Date: 2020/6/2 15:17
 * @Description:
 */
public interface IGatewayInterfaceServiceClient {

    @PostMapping(value = "/gateway/api/interface/userApiList")
    Result<List<SystemInterface>> queryUserApis(@RequestBody SystemUser params);

    @PostMapping(value = "/gateway/api/interface/apiList")
    Result<List<SystemInterfaceVO>> queryApiAllList(@RequestBody SystemInterfaceVO params);

    @PostMapping(value = "/gateway/api/interface/allApiList")
    Result<List<SystemInterface>> queryAllApi();

    @PostMapping(value = "/gateway/api/interface/limitApiList")
    Result<List<SystemInterfaceVO>> queryLimitApiList(@RequestBody SystemGatewayIplimitVO params);

}
