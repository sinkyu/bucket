package com.bucket.cloud.system.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "SystemUserAddModel", description = "用户角色")
@JsonIgnoreProperties(value = {"handler"})
public class SystemUserAddModel extends SystemUser implements Serializable {
    @ApiModelProperty(value = "角色数组")
    private String[] roleId;

    @ApiModelProperty(value = "权限数组")
    private String[] resourcesId;

    @ApiModelProperty(value = "职位数组")
    private String[] positionCode;

    @ApiModelProperty(value = "警种数组")
    private String[] policeCode;
}