package com.bucket.cloud.system.api.service;

import com.bucket.cloud.common.core.model.Result;
import com.bucket.cloud.system.api.model.SystemUser;
import com.bucket.cloud.system.api.vo.SystemUserVO;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @Auther: cyq
 * @Date: 2020/6/2 10:02
 * @Description: 系统用户接口
 */
public interface ISystemUserServiceClient {

    /**
     * @Author cyq
     * @Description 用户登陆接口
     * @Date 2020/8/14 6:33 下午
     * @param  * @param null
     * @return
    **/
    @PostMapping(value = "/system/user/login")
    Result<SystemUserVO> login(@RequestBody SystemUser u);
}
