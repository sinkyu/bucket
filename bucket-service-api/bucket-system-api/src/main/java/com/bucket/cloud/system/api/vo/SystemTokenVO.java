package com.bucket.cloud.system.api.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Auther: cyq
 * @Date: 2020/6/17 15:01
 * @Description: 系统token接收类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SystemTokenVO {

    private String token;
}
