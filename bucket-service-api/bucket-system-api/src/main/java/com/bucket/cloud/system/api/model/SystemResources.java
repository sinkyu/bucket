package com.bucket.cloud.system.api.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "SystemResources", description = "权限")
@JsonIgnoreProperties(value = {"handler"})
@TableName("SYSTEM_RESOURCES")
public class SystemResources  implements GrantedAuthority {

    private static final long serialVersionUID = 6551222365421448143L;

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "状态（0禁用，1启用）")
    private Integer state;

    @ApiModelProperty(value = "父id")
    private String pid;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "链接")
    private String url;

    @ApiModelProperty(value = "代码")
    private String code;

    @ApiModelProperty(value = "类型（菜单，按钮）")
    private String type;

    @ApiModelProperty(value = "按钮样式")
    @TableField(value = "BTN_CLASS")
    private String btnClass;

    @ApiModelProperty(value = "排序")
    private Integer sort;


    @Override
    public String getAuthority() {
        return this.code;
    }
}