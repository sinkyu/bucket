package com.bucket.cloud.system.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.bucket.cloud.common.core.model.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "SystemRole", description = "角色分页")
@JsonIgnoreProperties(value = {"handler"})
public class RolePage extends Page implements Serializable {

    private static final long serialVersionUID = 3582588209589180635L;

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "名称")
    private String name;

    @ApiModelProperty(value = "代码")
    private String code;

    @ApiModelProperty(value = "状态（0禁用，1启用）")
    private Integer state;
}