package com.bucket.cloud.system.api.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 系统应用-基础信息
 *
 * @author cyq
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class SystemAppClient implements Serializable {

    private static final long serialVersionUID = 6309560123291845697L;
    /**
     * 授权类型(多个使用,号隔开)
     */
    private String grantTypes;

    /**
     * 第三方应用授权回调地址
     */
    private String redirectUrls;

    /**
     * 用户授权范围(多个使用,号隔开)
     */
    private String scopes;

    /**
     * 令牌有效期(秒)
     */
    private Integer accessTokenValidity;

    /**
     * 刷新令牌有效期(秒)
     */
    private Integer refreshTokenValidity;

    /**
     * 用户自动授权范围(多个使用,号隔开)
     */
    private String autoApproveScopes;


}
