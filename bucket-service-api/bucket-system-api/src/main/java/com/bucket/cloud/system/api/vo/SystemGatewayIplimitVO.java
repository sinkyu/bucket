package com.bucket.cloud.system.api.vo;

import com.bucket.cloud.system.api.model.SystemGatewayIplimit;
import com.bucket.cloud.system.api.model.SystemInterface;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SystemGatewayIplimitVO extends SystemGatewayIplimit {

    private String intIds;

    private List<SystemInterface> interfaces;
}
