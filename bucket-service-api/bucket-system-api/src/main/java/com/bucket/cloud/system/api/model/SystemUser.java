package com.bucket.cloud.system.api.model;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "SystemUser", description = "用户")
@JsonIgnoreProperties(value = {"handler"})
@TableName("SYSTEM_USER")
public class SystemUser implements Serializable {

    private static final long serialVersionUID = -2128533438921937831L;

    @TableId
    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "账户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "加密盐")
    private String salt;

    @ApiModelProperty(value = "姓名、昵称")
    private String name;

    @ApiModelProperty(value = "状态（0禁用，1启用）")
    private Integer state;

    @ApiModelProperty(value = "身份证号")
    private String idcard;

    @ApiModelProperty(value = "现任单位")
    @TableField(value = "CURRENT_ORG")
    private String currentOrg;

    @ApiModelProperty(value = "临时单位")
    @TableField(value = "TEMPORARY_ORG")
    private String temporaryOrg;

    @ApiModelProperty(value = "联系电话")
    private String phone;

    @ApiModelProperty(value = "警号")
    @TableField(value = "POLICE_NUM")
    private String policeNum;

    @ApiModelProperty(value = "绑定微信的openid")
    private String openid;

    @ApiModelProperty(value = "登录类型:password-密码、mobile-手机号、email-邮箱、weixin-微信、weibo-微博、qq-等等")
    private String userType;

    @TableField(exist = false)
    private String deptName;

}