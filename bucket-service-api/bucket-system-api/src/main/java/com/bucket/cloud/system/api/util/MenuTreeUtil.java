package com.bucket.cloud.system.api.util;

import com.bucket.cloud.system.api.model.SystemMenu;

import java.util.ArrayList;
import java.util.List;

/**
 * @auther: cyq
 * @Description: 系统管理-菜单管理树结构返回
 * @Date: 2019/2/22 15:35
 * @param:
 * @return:
 */
public class MenuTreeUtil {

    public final static List<SystemMenu> getfatherNode(List<SystemMenu> treeDataList) {
        return getfatherNode(treeDataList, "0");
    }

    public final static List<SystemMenu> getfatherNode(List<SystemMenu> treeDataList, String parentCode) {
        List<SystemMenu> newTreeDataList = new ArrayList<SystemMenu>();
        for (SystemMenu systemMenu : treeDataList) {
            if(org.apache.commons.lang3.StringUtils.isBlank(systemMenu.getPid())
                    || systemMenu.getPid() .equals(parentCode)) {
                //获取父节点下的子节点
                systemMenu.setLists(getChildrenNode(systemMenu.getId(),treeDataList));
                systemMenu.setOpen(true);
                newTreeDataList.add(systemMenu);
            }
        }
        return newTreeDataList;
    }

    private final static List<SystemMenu> getChildrenNode(String pid , List<SystemMenu> treeDataList) {
        List<SystemMenu> newTreeDataList = new ArrayList<SystemMenu>();
        for (SystemMenu systemMenu : treeDataList) {
            if(org.apache.commons.lang3.StringUtils.isBlank(systemMenu.getPid()))  continue;
            //这是一个子节点
            if(systemMenu.getPid().equals(pid)){
                //递归获取子节点下的子节点
                systemMenu.setLists(getChildrenNode(systemMenu.getId() , treeDataList));
                newTreeDataList.add(systemMenu);
            }
        }
        return newTreeDataList;
    }
}