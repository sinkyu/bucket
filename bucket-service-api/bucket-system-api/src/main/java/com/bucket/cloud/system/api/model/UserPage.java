package com.bucket.cloud.system.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.bucket.cloud.common.core.model.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "UserPage", description = "用户分页")
@JsonIgnoreProperties(value = {"handler"})
public class UserPage extends Page implements Serializable {

    @ApiModelProperty(value = "id")
    private String id;

    @ApiModelProperty(value = "账户名")
    private String username;

    @ApiModelProperty(value = "密码")
    private String password;

    @ApiModelProperty(value = "加密盐")
    private String salt;

    @ApiModelProperty(value = "姓名、昵称")
    private String name;

    @ApiModelProperty(value = "状态（0禁用，1启用）")
    private Integer state;

    @ApiModelProperty(value = "身份证号")
    private String idcard;

    @ApiModelProperty(value = "现任单位")
    private String currentOrg;

    @ApiModelProperty(value = "临时单位")
    private String temporaryOrg;

    @ApiModelProperty(value = "联系电话")
    private String phone;

    @ApiModelProperty(value = "警号")
    private String policeNum;
}