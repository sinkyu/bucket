package com.bucket.cloud.system.api.service;

import com.bucket.cloud.system.api.model.SystemGatewayRouter;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

public interface IGatewayRouteServiceClient {

    @PostMapping(value = "/gateway/api/getRouterList")
    List<SystemGatewayRouter> selectByParams(SystemGatewayRouter params);

}
