package com.bucket.cloud.system.api.vo;

import com.bucket.cloud.system.api.model.SystemRole;
import com.bucket.cloud.system.api.model.SystemRoleResources;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SystemRoleVO extends SystemRole {

    private List<SystemRoleResources> systemRoleResourcesList;
}
