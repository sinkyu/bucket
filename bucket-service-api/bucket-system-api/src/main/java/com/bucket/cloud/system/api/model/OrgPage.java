package com.bucket.cloud.system.api.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.bucket.cloud.common.core.model.Page;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * code is far away from bug with the animal protecting
 * ┏┓　　　┏┓
 * ┏┛┻━━━┛┻┓
 * ┃　　　　　　　┃
 * ┃　　　━　　　┃
 * ┃　┳┛　┗┳　┃
 * ┃　　　　　　　┃
 * ┃　　　┻　　　┃
 * ┃　　　　　　　┃
 * ┗━┓　　　┏━┛
 * 　　┃　　　┃神兽保佑
 * 　　┃　　　┃代码无BUG！
 * 　　┃　　　┗━━━┓
 * 　　┃　　　　　　　┣┓
 * 　　┃　　　　　　　┏┛
 * 　　┗┓┓┏━┳┓┏┛
 * 　　　┃┫┫　┃┫┫
 * 　　　┗┻┛　┗┻┛
 *
 * @Description :
 * ---------------------------------
 * @Author : cyq
 * @Date : Create in 2018/11/14 18:41
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "OrgPage", description = "单位分页")
@JsonIgnoreProperties(value = {"handler"})
public class OrgPage extends Page implements Serializable {
    @ApiModelProperty(value = "id")
    private String id;
    @ApiModelProperty(value = "state")
    private Integer state;
    @ApiModelProperty(value = "code")
    private String code;
    @ApiModelProperty(value = "name")
    private String name;
    @ApiModelProperty(value = "pcode")
    private String pcode;
    @ApiModelProperty(value = "type")
    private String type;
    @ApiModelProperty(value = "shortName")
    private String shortName;
    @ApiModelProperty(value = "sort")
    private Integer sort;
}
