 **简介**
搭建基于OAuth2的开放平台、为APP端、应用服务提供统一接口管控平台、为第三方合作伙伴的业务对接提供授信可控的技术对接平台


- 分布式架构，Nacos(服务注册+配置中心)统一管理
- 统一API网关（参数验签、身份认证、接口鉴权、接口调试、接口限流、接口状态、接口外网访问）
- 统一oauth2认证
- sentinel限流、降权
- seata管理分布式事务

 **核心依赖** 
| 依赖  |  版本 |
|---|---|
| Spring Boot  | 2.2.9.RELEASE  |
| Spring Cloud | Hoxton.SR7  |
| Spring Cloud Alibaba | 2.2.0.RELEASE  |
|  Mybatis-plus | 3.3.2  |
|  Hutool | 4.1.19  |


需要开启mysql8、nacos、rabbitmq、redis：
![输入图片说明](https://images.gitee.com/uploads/images/2020/0810/214258_5e47427b_757994.png "屏幕截图.png")

 **服务开启顺序如下：** 

- BucketSystemApplication（系统管理服务）
- GatewayApplication（网关服务）
- CloudAuthApplication（认证中心服务）


 **网关管理：** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0810/212400_074633c3_757994.png "屏幕截图.png")

 **接口管理：** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0810/212221_c65f8d61_757994.png "屏幕截图.png")

 **白名单管理：** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0810/212251_affe61a8_757994.png "屏幕截图.png")

 **Springboot Admin监控** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0817/205636_f9eecac9_757994.png "屏幕截图.png")

 **sentinel** 
![输入图片说明](https://images.gitee.com/uploads/images/2020/0821/120404_bf5f6b6a_757994.png "屏幕截图.png")