package com.bucket.cloud.auth.service.feign;

import com.bucket.cloud.system.api.constant.SystemConstants;
import com.bucket.cloud.system.api.service.ISystemAppServiceClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

/**
 * @author: cyq
 * @date: 2018/10/24 16:49
 * @description:
 */
@Component
@FeignClient(value = SystemConstants.BASE_SERVER)
public interface BaseAppServiceClient extends ISystemAppServiceClient {


}
