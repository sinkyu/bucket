package com.bucket.cloud.auth.service.feign;

import com.bucket.cloud.system.api.constant.SystemConstants;
import com.bucket.cloud.system.api.service.IGatewayInterfaceServiceClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;

/**
 * @Auther: cyq
 * @Date: 2020/6/2 15:16
 * @Description:
 */
@Component
@FeignClient(value = SystemConstants.BASE_SERVER)
public interface GatewayInterfaceServiceClient extends IGatewayInterfaceServiceClient {
}
