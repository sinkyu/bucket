package com.bucket.cloud.monitor;

import de.codecentric.boot.admin.server.config.EnableAdminServer;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @Author cyq
 * @Description springboot admin监控
 * @Date 2020/8/17 6:38 下午
 **/
@EnableDiscoveryClient
@EnableAdminServer
@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)
public class SpringBootAdminApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootAdminApplication.class, args);
    }
}
